# Automated Host Commands Test Suite
Author: Megan Heydari, April 2021

### Step 1: Setting up to run on your machine 
1. Download the folder, *Automated Host Commands*
2. This folder will contain: 
    1. KeywordsAndVariables
        1. This directory contains the keyword library that the test suite references. This keyword library is named *HostCommands_KeywordLibrary.robot*.
    2. Tests
        1. This is where the test file is located that references the keywords to make up tests cases. 
    3. Results 
        1. This is the directory where the RF generated results .html files will be routed.
        
### Step 2: Modify filepaths to reflect your machine
##### Settings 
1. In Tests > HostCommandsTest.robot, you will need to update the "Resource" line with the path to *HostCommands_KeywordLibrary.robot* as it resides on your local machine. 
##### User Defined Variables
1. ${ssh_key}: update the path to your ssh key 
2. ${username}: update the username to reflect your host username
3. ${load}: this is just an example of using one load. I have put my load name in this variable. You can create multiple loads, as you like. 
4. ${host_ip}: update with your host's IP address
5. ${mode}: fill with a mode name. Your options are: 
    1. Money Saver Mode
    2. Island Mode
    3. Critical Load Mode 
    4. Storm Mode 
    5. Note that power outage mode has not been included. I do not think you can manually choose to enter this mode. 
    
### Step 3: Specify the results path and run the test suite 
1. Run the test suite by using the following format:  
`robot -d Results Tests/HostCommandsTest.robot`

2. This format assumes you are running the .robot from the AutomatedHostCommands directory, so change the path to the /Results and /Tests directories as you see fit. 

### Step 4: View Results
1. To view results, go to the Results folder and view the html file generated in your browser of choice. 

### Keyword Dictionary 
#### Establish ssh connection
* Inputs: host ip, ssh key path, host username 
* Function: Establishes ssh connection with host (or PBC if you choose a diff device).
* Returns: Nothing

#### Start ELM Sim
* Inputs: host ip, ssh key path, host username 
* Function: Starts running the elm simulator on the host.
* Returns: Nothing

#### Turn Load Off
* Inputs: host ip, load to turn off 
* Function: Turn the specified load off. Make sure that load label is exactly as you have it in your configuration. Spaces should be replaced with '%'.
* Returns: Nothing

#### Turn Load On
* Inputs: host ip, load to turn on 
* Function: Turn the specified load on. Make sure that load label is exactly as you have it in your configuration. Spaces should be replaced with '%'.
* Returns: Nothing

#### Check Power Reading
* Inputs: host ip, load to turn off 
* Function: Reads the power value of specified load. Make sure that load label is exactly as you have it in your configuration. Spaces should be replaced with '%'.
* Returns: Load power value. 

#### Check Energy Mode
* Inputs: host ip
* Function: Reads and returns the current mode the host is in. 
* Returns: The current mode the host is in 

#### Change Energy Mode
* Inputs: host ip, desired mode to change to 
    * Note that the mode must be presented as: 
        1. Money Saver Mode
        2. Island Mode
        3. Critical Load Mode 
        4. Storm Mode 
        5. Note that power outage mode has not been included. I do not think you can manually choose to enter this mode. 
* Function: Send command to host to put the system into the specified mode.  
* Returns: Nothing

#### Get JSON Value
* Inputs: json object, field
* Function: Converts the json object to a string. 
* Returns: String of the json object from the desired field.  

#### Get Battery Operating Mode
* Inputs: host ip 
* Function: Gets the current battery operating mode form the host and converts the code to human readable format.
* Returns: String of current battery operating mode.  

#### Read Microgrid Power Status
* Inputs: host ip 
* Function: prints out the solar power, battery power, battery operating mode, battery SOC, generator operating status, generator power to the console. 
* Returns: Nothing, but can easily be modified to return any of the above values. 

#### Hard Reboot Host
* Inputs: host ip 
* Function: Hard reboots the host and checks that the host can be ssh'd into after coming back up. Checks for an echo command to ensure host is up. Passes the test case if the host comes back up. Fails the test case if host does not come back up. 
* Returns: Nothing.  

#### Soft Reboot Host
* Inputs: host ip 
* Function: Soft reboots the host and checks that the host can be ssh'd into after coming back up. Checks for an echo command to ensure host is up. Passes the test case if the host comes back up. Fails the test case if host does not come back up. 
* Returns: Nothing.






