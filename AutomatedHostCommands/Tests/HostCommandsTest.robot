*** Settings ***
Documentation    This test suite experiments with reading and reporting power values from host and toggling loads
Resource         /Users/megan.heydari/Desktop/AutomatedHostCommands/KeywordsAndVariables/HostCommands_KeywordLibrary.robot      # <----- Your absolute path to the keyword library

*** Variables ***
# SSH Information
${ssh_key}              /Users/megan.heydari/.ssh/id_rsa        # <----- path to your ssh key here
${username}             RPM                                     # <----- host username here (likely RPM)

# Test Variables
${load}                 R1_CHA                       # <----- Enter your desired load here
${host_ip}              192.168.1.41                 # <----- Enter your host IP here
${mode}                 Storm Mode                   # <----- Enter mode you want to change to here

*** Test Cases ***
Start ELM Sim Session
    [Documentation]         Starts running the elm sim on the host. Uses SSH key for login.
    Start ELM Sim           ${host_ip}          ${ssh_key}          ${username}
    Sleep                   5s

Load Toggling
    [Documentation]         Turns a load off, checks that power is 0w. Turns same load on, prints power reading. Then turns off again and checks again.
    #turn load off
    Turn Load Off           ${host_ip}      ${load}
    ${off_1} =              Check Power Reading     ${host_ip}      ${load}
    #turn load on
    Turn Load On            ${host_ip}      ${load}
    ${on_1}                 Check Power Reading     ${host_ip}      ${load}
    #turn load off
    Turn Load Off           ${host_ip}      ${load}
    ${off_2} =              Check Power Reading     ${host_ip}      ${load}
    # pass/fail logic
#    Pass Execution If       ${off_1} == 0 and ${on_1} != 0 and ${off_2} == 0    ${load} responded to host commands. OFF > ON > OFF
    Should Be Equal as Integers     ${off_1}     0      ${load} responded to host commands. OFF
    Should Not Be Equal as Integers     ${on_1}      0      ${load} responded to host commands.  ON
    Should Be Equal as Integers     ${off_2}     0      ${load} responded to host commands. OFF
Changing Mode
    [Documentation]         Checks mode, prints mode, changes mode, prints new mode. You must have simulator running to execute this test.
    # Check original mode.
    ${mode_1} =             Check Energy Mode   ${host_ip}
    # Change Energy Mode
    Change Energy Mode      ${host_ip}  ${mode}
    ${mode_2} =             Check Energy Mode   ${host_ip}
    # Pass/Fail Logic
    ${equal} =              Should Be Equal As Strings    ${mode_2}    ${mode}
    Pass Execution if       ${equal} == 1                 PASS

Microgrid Power Reading
    [Documentation]     Returns the grid, solar, battery, battery SOC, pml, and generator power values of the system
    Read Microgrid Power Status     ${host_ip}

Soft Reboot Host
    [Documentation]     Soft Reboots the host
    Soft Reboot Host    ${host_ip}
    Hard Reboot Host    ${host_ip}

Hard Reboot Host
    [Documentation]     Hard Reboots the host
    Hard Reboot Host    ${host_ip}

Close Connections
    [Documentation]     Closes ssh connections established during test suite
    SSHLibrary.Close all connections