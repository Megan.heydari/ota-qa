#M. Heydari 
#April 2021 

*** Settings ***
Library  Telnet
Library  Process
Library  SCPLibrary
Library  SSHLibrary
Library  String
Library  OperatingSystem
Library   Collections

*** Keywords ***
#------------------------------------------------------------------------------------------------------------------------------------------------
# Prerequisite
Establish ssh connection
    [Documentation]                     Establishes ssh connection with the specified IP address
    [Arguments]                         ${ip}              ${ssh_key}          ${username}
    SSHLibrary.Open Connection          ${ip}              encoding=latin-1    timeout=15s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}

Start ELM Sim
    [Documentation]             Runs the simulator on the host
    [Arguments]                 ${host_ip}          ${ssh_key}          ${username}
    Establish ssh connection    ${host_ip}          ${ssh_key}          ${username}
    start command               cd ~/Applications/RacePointMedia/ ; ./elmSim
    Log to Console              \n ELM Sim started on host ${host_ip}
#------------------------------------------------------------------------------------------------------------------------------------------------

# Host, load state, mode state, power readings
Turn Load Off
    [Documentation]             Turns specified load off
    [Arguments]                 ${host_ip}   ${load}
    run process                 curl http://${host_ip}:3050/control/energy/loads/${load}/Off  shell=yes
    Sleep                       5s
    Log to Console              \n${load} OFF

Turn Load On
    [Documentation]             Turns specified load on
    [Arguments]                 ${host_ip}   ${load}
    run process                 curl http://${host_ip}:3050/control/energy/loads/${load}/On  shell=yes
    Sleep                       5s
    Log to Console              \n${load} On

Check Power Reading
    [Documentation]             Returns load status
    [Arguments]                 ${host_ip}   ${load}
    ${power} =                  run process     curl http://${host_ip}:3050/states/Energy.Circuit.${load}.Power    shell=yes
    ${power_final} =            Get JSON Value      ${power.stdout}      data
    Log to Console              \n${load} Power: ${power_final} watts
    [return]                    ${power_final}

Check Energy Mode
    [Documentation]             Checks energy mode
    [Arguments]                 ${host_ip}
    ${out} =                    run process     curl http://${host_ip}:3050/states/Energy.ModeName    shell=yes
    ${mode} =                   Get JSON Value      ${out.stdout}      data
    Log to Console              \nCurrent Mode: ${mode}
    [return]                    ${mode}

Change Energy Mode
    [Documentation]             Changes energy mode to desired mode. Converts written mode to numerical mode
    [Arguments]                 ${host_ip}      ${mode}
    ${mode_code} =	Set Variable If
    ...	'${mode}' == 'Money Saver Mode'	 0
    ...	'${mode}' == 'Power Outage Mode'	1
    ...	'${mode}' == 'Island Mode'	2
    ...	'${mode}' == 'Critical Load Mode'	3
    ...	'${mode}' == 'Storm Mode'	4
    run process                 curl http://${host_ip}:3050/control/energy/setmode/${mode_code}   shell=yes
    Log to Console              \nMode change to ${mode}

Get JSON Value
    [Documentation]             Returns the JSON contents of field from JSON object
    [Arguments]                 ${json obj}     ${field}
    ${string json} =            evaluate    json.loads('''${json obj}''')    json
    ${result} =	                Set Variable    ${string json['${field}']}
    [return]                    ${result[0]}

Get Battery Operating Mode
    [Documentation]             Queries the host for battery operating mode, converts mode code to code name
    [Arguments]                 ${host_ip}
    ${out} =                    run process     curl http://${host_ip}:3050/states/Energy.Battery.OperatingMode  shell=yes
    ${BatOpMode} =              Get JSON Value      ${out.stdout}      data
    ${BatOpModeName} =          Run Keyword if              ${BatOpMode}[0] == 0        Set Variable    IDLE
    ${BatOpModeName} =          Run Keyword if              ${BatOpMode}[0] == 1        Set Variable    Charging
    ${BatOpModeName} =          Run Keyword if              ${BatOpMode}[0] == 2        Set Variable    Discharging
    ${BatOpModeName} =	        Set Variable If
    ...	${BatOpMode} == 0   IDLE
    ...	${BatOpMode} == 1	Charging
    ...	${BatOpMode} == 2	Discharging
    Log to console              \nBattery is ${BatOpModeName}
    [return]                    ${BatOpModeName}

Read Microgrid Power Status
    [Documentation]             Prints microgrid power values
    [Arguments]                 ${host_ip}
    # Grid Power
    # Solar Power
    ${solar_power} =            run process     curl http://${host_ip}:3050/states/Energy.Circuit.Solar.Power  shell=yes
    ${solar_power_json} =       Get JSON Value      ${solar_power.stdout}      data
    Log to Console              \nSolar Power: ${solar_power_json} w
    # Battery Power, Battery SOC
    ${SOC} =                    run process     curl http://${host_ip}:3050/states/Energy.Battery.StateOfCharge  shell=yes
    ${SOC_json} =               Get JSON Value      ${SOC.stdout}      data
    Log to Console              \nBattery SOC: ${SOC_json} %
    ${battery_power} =          run process     curl http://${host_ip}:3050/states/Energy.Circuit.Battery.Power  shell=yes
    ${battery_power_json} =     Get JSON Value      ${battery_power.stdout}      data
    ${bat_operating_mode} =     Get Battery Operating Mode  ${host_ip}
    Log to Console              \nBattery Power: ${battery_power_json} w
    # Generator Power
    ${gen_power} =              run process     curl http://${host_ip}:3050/states/Energy.Circuit.Generator.Power  shell=yes
    ${gen_power_json} =         Get JSON Value      ${gen_power.stdout}      data
    ${gen_status} =	        Set Variable If
    ...	${gen_power_json} == 0   OFF
    ...	${gen_power_json} != 0	ON
    Log to Console              \nGenerator Status: ${gen_status}
    Log to Console              \nGenerator Power: ${gen_power_json} w
    Log to Console              \n

Hard Reboot Host
    [Documentation]             Hard reboots the host
    [Arguments]                 ${host_ip}
    run process                 curl http://${host_ip}:3050/private/hardreboot  shell=yes
    Log to console              \n Rebooting...
    Sleep                       30s
    Establish ssh connection    ${host_ip}          ${ssh_key}          ${username}
    ${out} =                    SSHLibrary.execute command             echo Hello World
    Pass execution if           """${out}""" == """Hello World"""    Pass, host hard rebooted and is online
    Fail                        Host did not come back up

Soft Reboot Host
    [Documentation]             Soft reboots the host
    [Arguments]                 ${host_ip}
    run process                 curl http://${host_ip}:3050/private/softreboot  shell=yes
    Log to console              \n Rebooting...
    Sleep                       10s
    Establish ssh connection    ${host_ip}          ${ssh_key}          ${username}
    ${out} =                    SSHLibrary.execute command             echo Hello World
    Pass execution if           """${out}""" == """Hello World"""    Pass, host soft rebooted and is online
    Fail                        Host did not come back up


#------------------------------------------------------------------------------------------------------------------------------------------------