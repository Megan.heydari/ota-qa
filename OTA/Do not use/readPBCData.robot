# M. Heydari, RPE, PCB Automation testing 09/22/20
#Out of date, don't use
*** Settings ***
Library  Telnet
Library  Process
Library  SSHLibrary
Library  String
*** Variables ***
${pbc}              192.168.1.31
${username}         rpe
${password}         rpe

${SE}               SET_ENERGY=.*



*** Test Cases ***
#Trigger OTA update through telnet
Establish telnet connection at port 2000
    Telnet.Open Connection                      ${pbc}  port=2000  terminal_emulation=True	terminal_type=vt100	window_size=400x100

Read latest SET_ENERGY Listing
    sleep  1s
    set prompt          ${SE}
    Telnet.Read
#    ${Reading}=  Telnet.Read until prompt  loglevel=DEBUG
#    ${string} =	Decode Bytes To String	${Reading}	JSON

#Convert reading to from 64 bit to JSON

#SSH into PBC and verify version is correct
#    SSHLibrary.Open Connection    ${pbc}        alias=${alias}
#    SSHLibrary.Login              ${username}    ${password}    delay=1
#    ${stdout}=         SSHLibrary.Execute Command    ${version check}  sudo=True   sudo_password=${password}
#    Log    ${stdout}
#    SSHLibrary.close all connections


*** Keywords ***
