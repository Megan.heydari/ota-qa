# M. Heydari, RPE, OTA Automation testing 10/12/20
*** Settings ***
Library  Telnet
Library  Process
Library  SSHLibrary
Library  String
Library  Process
Library  OperatingSystem
Library  ./extractVersion.py
Library  ./extractOTAProgress.py


*** Variables ***
${pbc}              192.168.1.36
${username}         rpe
${password}         rpe

${SETprompt}        gateway SET_OTA*
${updateComplete}   SET_OTA_PROGRESS=eyJtZXNzYWdlIjoiVXBkYXRlIGNvbXBsZXRlLiIsInN0YWdlIjoiaWRsZSJ9
${updateFailed}     SET_OTA_PROGRESS=eyJzdGFnZSI6ImVycm9yIiwibWVzc2FnZSI6Ik5vIHNhdGlzZnlpbmcgdmVyc2lvbiBhdmFpbGFibGUuIn0=
#${updateFailed}     SET_OTA_PROGRESS=eyJtZXNzYWdlIjoiTm8gc2F0aXNmeWluZyB2ZXJzaW9uIGF2YWlsYWJsZS4iLCJzdGFnZSI6ImVycm9yIn0=
#${upToDate}         SET_OTA_PROGRESS=eyJzdGFnZSI6ImVycm9yIiwibWVzc2FnZSI6Ik5vIHNhdGlzZnlpbmcgdmVyc2lvbiBhdmFpbGFibGUuIn0=
${noVersion}        SET_OTA_PROGRESS=eyJtZXNzYWdlIjoiTm8gc2F0aXNmeWluZyB2ZXJzaW9uIGF2YWlsYWJsZS4iLCJzdGFnZSI6ImVycm9yIn0=
${upToDate}         gateway SET_OTA_PROGRESS=eyJtZXNzYWdlIjoiR2F0ZXdheXMgYXJlIGFscmVhZHkgb24gdGhlIGxhdGVzdCB1cGRhdGUuIiwic3RhZ2UiOiJpZGxlIn0=
${gatewayVersion}
${currVersion}

${USERNAME}         rpe
${PASSWORD}         rpe
${updateComplete}

*** Test Cases ***
#Trigger OTA update through telnet
Establish telnet connection at port 2000
    Telnet.Open Connection           ${pbc}  port=2000  terminal_emulation=True	terminal_type=vt100	window_size=400x100

Ask for Gateway version
    sleep  1s
    set prompt                      ${SETprompt}
    Telnet.Write                    @gateway GET_OTA
    ${gatewayVersion}=              Telnet.Read
    Set Global Variable             ${gatewayVersion}
    Log                             ${gatewayVersion}

Extract Gateway version number
    ${currVersion}=                 extractVersion      ${gatewayVersion}
    Log                             ${currVersion}
    Set Global Variable             ${currVersion}

Change Gateway version
    SSHLibrary.Open Connection      ${pbc}
    SSHLibrary.Login                ${USERNAME}        ${PASSWORD}
    SSHLibrary.Execute Command      sudo systemctl restart prudmxagent  sudo_password=rpe

Trigger gateway OTA
#    SSHLibrary.Open Connection      ${pbc}
#    SSHLibrary.Login                ${USERNAME}        ${PASSWORD}
    SSHLibrary.Execute Command      sudo systemctl restart prudmxagent  sudo_password=rpe
    Telnet.Write                    @gateway SET_OTA=X.X.X
    set prompt                      ${updateComplete}
    ${report}=  Telnet.Read Until Regexp        ${updateComplete}        ${upToDate}    ${updateFailed}     ${noVersion}
    ${updateStatus}=                extractOTAProgress      ${report}

