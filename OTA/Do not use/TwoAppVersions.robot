*** Settings ***
Library  Telnet
Library  Process
Library  SCPLibrary
Library  SSHLibrary
Library  String
Library  OperatingSystem
Library  ./ExtractGWVersion.py

*** Variables ***
# pbc
${pbc}                      192.168.1.18
${username}                 rpe
${password}                 rpe
${nordic1}                  C8B1C66CBB9F      #MAC address
${nordic2}                  E292564D4837      #MAC address

# file locations
${app1Loc}                  /Users/megan.heydari/Desktop/ota-qa/gwApp/Companion-Gateway-Bootloader-Package-V1.0.8.zip
${app2Loc}                  /Users/megan.heydari/Desktop/ota-qa/gwApp/Companion-Gateway-Bootloader-Package-V0.1.19.zip
${app1}                     Companion-Gateway-Bootloader-Package-V1.0.8.zip
${app2}                     Companion-Gateway-Bootloader-Package-V0.1.19.zip
#${release1}                 /Users/megan.heydari/Desktop/ota-qa/releases/release1.json
#${release2}                 /Users/megan.heydari/Desktop/ota-qa/releases/release2.json
${nordic1INT}               /home/rpe/Nordic1_INT.sh
${nordic2INT}               /home/rpe/Nordic2_INT.sh
${release}                  /etc/rpe/FirmwareRelease/release.json
${release1}                 /home/rpe/releases/release1.json
${release2}                 /home/rpe/releases/release2.json

# pbc directories
${companionGateway}         /etc/rpe/FirmwareRelease/CompanionGateway
${releaseDir}               /etc/rpe/FirmwareRelease
${homeDir}                  /home/rpe

# OTA commands
${getGatewayVer}            @gateway GET_OTA
${restartPruDmxManager}     sudo systemctl restart prudmxagent
${startGatewayOTA}          @gateway SET_OTA=X.X.X
${logs}                     journalctl -f

# PBC Regular Commands
${startDMX}                 sudo systemctl start prudmxagent
${stopDMX}                  sudo systemctl stop prudmxagent
${restartDMX}               sudo systemctl restart prudmxagent


*** Test Cases ***
SCP 2 gw App version to PBC
    SCPLibrary.Open Connection   ${pbc}         username=${username}      password=${password}
    SCPLibrary.Put File          ${app1Loc}     ${homeDir}
    SCPLibrary.Put File          ${app2Loc}     ${homeDir}
    SCPLibrary.Close Connection


Move both files to CompanionGateway directory
    ${app1_HomePath} =   Catenate    SEPARATOR=/  ${homeDir}    ${app1}
    ${app2_HomePath} =   Catenate    SEPARATOR=/  ${homeDir}    ${app2}
    Log                  ${app1_HomePath}
    Log                  ${app2_HomePath}
    SSHLibrary.Open Connection      ${pbc}
    SSHLibrary.Login                ${username}         ${password}
    ${mvApp1} =   Catenate  sudo  mv  ${app1_HomePath}  ${companionGateway}
    ${mvApp2} =   Catenate  sudo  mv  ${app2_HomePath}  ${companionGateway}
    SSHLibrary.Execute Command  ${mvApp1}  sudo=True  sudo_password=rpe
    SSHLibrary.Execute Command  ${mvApp2}  sudo=True  sudo_password=rpe
    SSHLibrary.Close Connection

OTA from 1.0.8 > 0.1.19
    ${currVersion} = 1.0.8
    ${goalVersion} = 0.1.19
    #Get current app version, save version
    ${startingAppVersion_Nordic1} =         Get App Version         ${pbc}      ${nordic1}
    ${startingAppVersion_Nordic2} =         Get App Version         ${pbc}      ${nordic2}
    #Modify release.json to trigger update
    Change Release                                          ${release1}
    #Trigger OTA in telnet, wait for success/failure
    Trigger OTA
    #Check new gw app version
    ${newAppVersion_Nordic1} =              Get App Version         ${pbc}       ${nordic1}
    ${newAppVersion_Nordic2} =              Get App Version         ${pbc}       ${nordic1}
    #Compare app version before and after OTA
    Compare gw app versions                 ${startingAppVersion_Nordic1}        ${newAppVersion_Nordic1}       ${goalVersion}
    Compare gw app versions                 ${startingAppVersion_Nordic2}        ${newAppVersion_Nordic2}       ${goalVersion}


OTA from 0.1.19 > 1.0.8
    ${currVersion} = 0.1.19
    ${goalVersion} = 1.0.8
    #Get current app version, save version
    ${startingAppVersion_Nordic1} =         Get App Version         ${pbc}      ${nordic1}
    ${startingAppVersion_Nordic2} =         Get App Version         ${pbc}      ${nordic2}
    #Modify release.json to trigger update
    Change Release                                          ${release2}
    #Trigger OTA in telnet, wait for success/failure
    Trigger OTA
    #Check new gw app version
    ${newAppVersion_Nordic1} =              Get App Version         ${pbc}       ${nordic1}
    ${newAppVersion_Nordic2} =              Get App Version         ${pbc}       ${nordic1}
    #Compare app version before and after OTA
    Compare gw app versions                 ${startingAppVersion_Nordic1}        ${newAppVersion_Nordic1}       ${goalVersion}
    Compare gw app versions                 ${startingAppVersion_Nordic2}        ${newAppVersion_Nordic2}       ${goalVersion}


*** Keywords ***
Get App Version
    [Arguments]                         ${pbc}              ${gw}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1
    SSHLibrary.Login                    ${username}         ${password}
    SSHLibrary.Execute Command          systemctl stop prudmxagent  sudo=True  sudo_password=${password}
    ${gwInt}                            Catenate  /home/rpe/cgw-testtool /dev/ttyS2 ${gw} int
    ${gwInt}                            Convert To String   ${gwInt}
    ${stdout} =                         SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${gw} int  sudo=True  sudo_password=rpe
    ${Version} =                        extractGWVersion    ${stdout}
    [return]                            ${Version}

Change Release
    [Arguments]                         ${app}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1
    SSHLibrary.Login                    ${username}         ${password}
    SSHLibrary.Execute Command          sudo cp ${app} ${release}  sudo=True  sudo_password=${password}

Trigger OTA
    [Arguments]

    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1
    SSHLibrary.Login                    ${username}         ${password}
    SSHLibrary.Execute Command          systemctl restart prudmxagent  sudo=True  sudo_password=${password}
    Telnet.Write                        @gateway SET_OTA=X.X.X
    SSHLibrary.Execute Command          journalctl -f  sudo=True  sudo_password=${password}
    SSHLibrary.Read Until Regexp        prudmxagent.service: Succeeded.
    [status]

OTA Status Check
    [Arguments]                         ${report}
    ${success}                          prudmxagent.service: Succeeded.
    ${failure}                          Warning: Error getting release: The operation could not be completed.
    ${upToDate}
    Run Keyword If                      ${report} == ${success}  Successful OTA
                                        ...  Else If ${report} == ${upToDate}  Up To Date OTA
                                        ...  Else  Failed OTA

Compare gw app versions
    [Arguments]                         ${ver1}             ${ver2}         ${goal}
    Run Keyword If                      ${ver1} != ${ver2} and ${ver2} == ${goal}  Successful OTA
                                        ...  Else if  ${ver1} == ${goal}           Up To Date OTA
                                        ...  Else                                  Failed OTA
Successful OTA
    Log To Console                      OTA Successful
Up To Date OTA
    Log To Console                      App FW already up to date
Failed OTA
    Log To Console                      OTA Failed

Version Change
    Log to Console                      Version Changed

No Version Change
    Log to Console                      No Version Change