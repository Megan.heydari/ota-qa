*** Settings ***
Library  ./pythonHelpers/checkNordicStatus.py
Library   ./pythonHelpers/numCompanions.py
Library  SSHLibrary
Library  ImageHorizonLibrary
Library  ./pythonHelpers/ExtractGWVersion.py
Library  Process
Library  OperatingSystem



*** Variables ***
${pbc_ip}                   192.168.1.44
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${failures}                 0

*** Test Cases ***

Test case with loop assertion

#    Establish ssh connection    ${pbc_ip}
#    Sleep                       10s

    Reboot PBC                  ${pbc_ip}
    run process                 ssh root@${pbc_ip} systemctl stop prudmxagent  shell=yes
    Reset                       ${pbc_ip}  ${nordic1}
    Reset                       ${pbc_ip}  ${nordic2}
    FOR   ${var}  IN RANGE  3

         Log to Console          /////////////////////////////////////////////
         Log to Console          Attempt ${var}

#         Reboot PBC                  ${pbc_ip}
#         Establish ssh connection    ${pbc_ip}
#         SSHLibrary.Execute Command  systemctl stop prudmxagent

         ${V1}=                 Run Keyword And Continue On Failure     BL INT  ${pbc_ip}   ${nordic1}
         ${V2}=                 Run Keyword And Continue On Failure     BL INT  ${pbc_ip}   ${nordic2}
#         Log to Console  ${V1}
#         Log to Console  ${V2}
#         Run Keyword unless     '${V1}' == '0.0.0'    Counter     ${failures}
#         Run Keyword unless     '${V2}' == '0.0.0'    Counter     ${failures}

    END

    SSHLibrary.close all connections
#    Print Results   ${var}


*** Keywords ***

Establish ssh connection
    [Arguments]                         ${pbc}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1    timeout=3s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}

Reboot PBC
    [Arguments]                 ${pbc_ip}
    #SSH into PBC
#    Log to Console              Establish ssh connection
    Establish ssh connection    ${pbc_ip}
#    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
    SSHLibrary.Execute Command  reboot
    Sleep                       60s
    SSHLibrary.close all connections


BL INT
    [Arguments]                 ${pbc_ip}   ${MAC}
    [Timeout]                   90 s

#    run process                 ssh root@${pbc_ip} systemctl stop prudmxagent  shell=yes

#    ${in1} =                    Set Variable  ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5; sleep 2s
#    Run Keyword And Continue On Failure     start process                 ${in1}  shell=yes

    ${in2} =                    Set Variable  ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int
    ${result} =                 Wait Until Keyword Succeeds 	20s	 3s      run process   ${in2}                shell=yes

#    Log to console              ${result.stdout}
    ${Version} =                extractGWVersion    ${result.stdout}
    Log to Console              ${MAC}: ${Version}
    [Return]                    ${Version}


Counter
    [Arguments]                 ${in}
    ${in} =                     Evaluate  ${in}+1

Print Results
    [Arguments]                 ${var}
    ${finalVar} =               Evaluate    ${var} + ${var} + 1
    Log to Console          Out of ${finalVar} cases, ${failures} cases failed

Run INT


Reset
    [Arguments]     ${pbc_ip}  ${MAC}
    ${in1} =                    Set Variable  ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5; sleep 2s
    Run Keyword And Continue On Failure     start process                 ${in1}  shell=yes

