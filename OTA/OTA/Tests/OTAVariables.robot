*** Variables ***
# PBC Info
${pbc_ip}                   192.168.1.35
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
#${nordic1}                  C8B1C66CBB9F      #MAC address, 1000
#${nordic2}                  E292564D4837      #MAC address, 1000

${host_ip}                  192.168.1.41

# SSH Info
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${app_dir_remote}           /etc/rpe/FirmwareRelease/CompanionGateway
# App file directory info
${app_local}                /Users/megan.heydari/Desktop/ota-qa/gwApp  #change the file path to reflect the file you want
${app19}                    Companion-Gateway-Bootloader-Package-V0.1.19.zip
${app20}                    Companion-Gateway-Bootloader-Package-V0.1.20.zip
${release_path}             /etc/rpe/FirmwareRelease/release.json
${local_release}            ./Releases/release.json
${HVAC}                     HVAC-Bootloader-Package-V0.0.7.zip