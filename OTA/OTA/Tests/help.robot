*** Settings ***
Documentation    Suite description
Library  Telnet
Library  Process
Library  SCPLibrary
Library  SSHLibrary
Library  String
Library  OperatingSystem
Library  ./pythonHelpers/ExtractGWVersion.py
Library  ./pythonHelpers/ExtractVersionFileName.py
Library   Collections


*** Variables ***
${pbc_ip}                   192.168.1.25
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${app_dir_remote}           /etc/rpe/FirmwareRelease/CompanionGateway
${app_local}                /Users/megan.heydari/Desktop/ota-qa/gwApp  #change the file path to reflect the file you want
${app19}                      Companion-Gateway-Bootloader-Package-V0.1.19.zip
${app20}                      Companion-Gateway-Bootloader-Package-V0.1.20.zip
${release_path}             /etc/rpe/FirmwareRelease/release.json
${local_release}            ./Releases/release.json


*** Test Cases ***
Test 1
    FOR   ${var}  IN RANGE  1
        Log to Console          /////////////////////////////////////////////
        Log to Console          Attempt ${var}

        Run Keyword And Continue On Failure             Update App and Check Version    ${pbc_ip}   ${app20}
        Run Keyword And Continue On Failure             Update App and Check Version    ${pbc_ip}   ${app19}
    END


#Reboot PBC 1
#    #SSH into PBC
#    Log to Console              Establish ssh connection
#    Establish ssh connection    ${pbc_ip}
##    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
#    SSHLibrary.Execute Command  reboot
#    Sleep                       60s
#
#
#Update Gateway App to 1.20
#    #SSH into PBC
#    Log to Console              Establish ssh connection
#    Establish ssh connection    ${pbc_ip}
#    #Copy desired gw app file to pbc
#    Log to console              Moving app to pbc
#    Move app file               ${app_local}        ${app_dir_remote}       ${app20}
#    #Modify release.json to match your app
#    Log to console              Modifying release.json to match app
#    Modify release.json         ${app20}              ${release_path}          ${app_dir_remote}
#    ${version1_before}=  Get App Version PBC         ${nordic1}
#    ${version2_before}=  Get App Version PBC         ${nordic2}
#    Log to Console              Version Before 0: ${version1_before}\n
#    Log to Console              Version Before 1: ${version2_before}\n
#    Log to Console              Triggering the OTA
#    Trigger OTA                 ${pbc_ip}
#
#Reboot PBC 2
#    #SSH into PBC
#    Log to Console              Establish ssh connection
#    Establish ssh connection    ${pbc_ip}
##    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
#    SSHLibrary.Execute Command  reboot
#    Sleep                       60s
#
#Check Version 1.20
#    Log to Console              Checking Version
#    Establish ssh connection    ${pbc_ip}
#    ${version1_after}=  Get App Version PBC         ${nordic1}
#    ${version2_after}=  Get App Version PBC         ${nordic2}
#    Log to console                     Version After 0: ${version1_after}\n
#    Log to console                     Version After 1: ${version2_after}\n
#    Log                                Version After 0: ${version1_after}
#    Log                                Version After 1: ${version2_after}
#    SSHLibrary.close all connections
#
##Reboot PBC
##    #SSH into PBC
##    Log to Console              Establish ssh connection
##    Establish ssh connection    ${pbc_ip}
###    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
##    SSHLibrary.Execute Command  reboot
##    Sleep                       60s
#
#Update Gateway App to 1.19
#    #SSH into PBC
#    Log to Console              Establish ssh connection
#    Establish ssh connection    ${pbc_ip}
#    #Copy desired gw app file to pbc
#    Log to console              Moving app to pbc
#    Move app file               ${app_local}        ${app_dir_remote}       ${app19}
#    #Modify release.json to match your app
#    Log to console              Modifying release.json to match app
#    Modify release.json         ${app19}              ${release_path}          ${app_dir_remote}
#    Log to Console              Getting App Version Before
#    ${version1_before}=  Get App Version PBC         ${nordic1}
#    ${version2_before}=  Get App Version PBC         ${nordic2}
#    Log to Console              Version Before 0: ${version1_before}\n
#    Log to Console              Version Before 1: ${version2_before}\n
#    Log                         Version Before 0: ${version1_before}
#    Log                         Version Before 1: ${version2_before}
#    Log to Console              Triggering the OTA
#    Trigger OTA                 ${pbc_ip}
#
#Reboot PBC 3
#    #SSH into PBC
#    Log to Console              Establish ssh connection
#    Establish ssh connection    ${pbc_ip}
##    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
#    SSHLibrary.Execute Command  reboot
#    Sleep                       60s
#
#Check version for 1.19
#    Log to Console              Checking Version
#    Establish ssh connection    ${pbc_ip}
#    ${version1_after}=  Get App Version PBC         ${nordic1}
#    ${version2_after}=  Get App Version PBC         ${nordic2}
#    Log to console                     Version After 0: ${version1_after}\n
#    Log to console                     Version After 1: ${version2_after}\n
#    Log                                Version After 0: ${version1_after}
#    Log                                Version After 1: ${version2_after}
#    SSHLibrary.close all connections




*** Keywords ***
Establish ssh connection
    [Arguments]                         ${pbc}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1
    SSHLibrary.Login with public key    ${username}         ${ssh_key}
Move app file
#    Move local app file to correct location on PBC
    [Arguments]                         ${local}            ${remote}       ${app}
    ${local_path} =                     Catenate    SEPARATOR=/  ${local}   ${app}
    SSHLibrary.Put File                 ${local_path}            ${remote}  scp=ALL

Modify release.json
#    Modify release.json to point to the new app
    [Arguments]                         ${app}              ${release}          ${app_dir_remote}
    ${new_version} =                    extractVersionFile                  ${app}
    SSHLibrary.Get File                 ${release_path}     ${local_release}    scp=ALL
    ${json_string}=    OperatingSystem.Get File     ${local_release}
    ${json_obj}=    evaluate    json.loads('''${json_string}''')    json
    ${remote_path}=  Catenate       SEPARATOR=/  ${app_dir_remote}   ${app}
    set to dictionary    ${json_obj["gateway"]}   path=${remote_path}
    set to dictionary    ${json_obj["gateway"]}   version=${new_version}
    ${json_string}=    evaluate    json.dumps(${json_obj})    json
#    log to console       \nNew JSON string:\n${json_string}
    Create File    ./Releases/release.json    ${json_string}    UTF-8
    SSHLibrary.Put File               ./Releases/release.json   /etc/rpe/FirmwareRelease    scp=ALL

Trigger OTA
#    Trigger the OTA in telnet
    [Arguments]                         ${pbc}
    Telnet.Open Connection              ${pbc}  port=2000
    SSHLibrary.Execute Command          systemctl restart prudmxagent
    SSHLibrary.Start Command            journalctl -f
    sleep                               60s
    Telnet.Write                        @gateway SET_OTA=X.X.X
    sleep                               400s


Get App Version PBC
#    Confirm version number has changed using cgw-testtool
    [Arguments]                         ${uid}
#    SSHLibrary.Execute Command          systemctl stop prudmxagent
    ${List}=                         Wait Until Keyword Succeeds  10 s    30ms   SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${uid} int   return_stdout=True     return_rc=True  timeout=1s
    ${result}=  Get From List   ${List}     0
    ${rc}=      get from list   ${List}     1
    ${Version} =                        extractGWVersion    ${result}
    [return]                            ${Version}  ${rc}


Redo Restart
    [Arguments]

    FOR    ${i}    IN RANGE    999999
            Establish ssh connection    ${pbc_ip}
           SSHLibrary.Execute Command          systemctl restart prudmxagent
           Sleep                     15s
           SSHLibrary.Execute Command          systemctl stop prudmxagent

           ${version1_before}  ${rc0}=  Run Keyword and Continue on Failure     Get App Version PBC         ${nordic1}
           ${version2_before}  ${rc1}=  Run Keyword and Continue on Failure     Get App Version PBC         ${nordic2}
           Exit For Loop If    ${rc0} == 0 and ${rc1} == 0
           Log    ${i}
    END
    Log    Exited
    [Return]    ${version1_before}  ${rc0}  ${version2_before}  ${rc1}

Update App and Check Version
    [Arguments]                ${pbc_ip}    ${app}

    #UPDATE APP
    #SSH into PBC
    Establish ssh connection    ${pbc_ip}
    SSHLibrary.Execute Command  reboot
    Sleep                       60s

    #UPDATE APP
#    Establish ssh connection    ${pbc_ip}

#    SSHLibrary.Execute Command          systemctl stop prudmxagent

    ${version1_before}  ${rc0}  ${version2_before}  ${rc1}=     Redo Restart
#    ${version1_before}  ${rc0}=  Get App Version PBC         ${nordic1}
#    ${version2_before}  ${rc1}=  Get App Version PBC         ${nordic2}


    Log to Console              Return Code 0: ${rc0}
    Log to Console              Version Before 0: ${version1_before}\n
    Log to Console              Version Before 1: ${version2_before}\n
    Log to Console              Triggering the OTA

    SSHLibrary.close all connections

