#PBC Linux OTA Cycle Test
#    [Documentation]  Update and downgrade PBC Linux OTA
#    FOR   ${var}  IN RANGE  1
#        Run Keyword if          ${var} == 0     Log to Console  \n
#        Log to Console          Attempt ${var}
#        Run Keyword And Warn On Failure         Linux OTA   ${pbc_ip}   ${210_ver}
#        Log to Console    \n
#        Run Keyword And Warn On Failure         Linux OTA   ${pbc_ip}   ${202_Ver}
#        Log to console    \n
#    END


*** Keywords ***
#Linux OTA
#    [Documentation]     Updates PBC Linux FW to specified version
#    [Arguments]         ${pbc_ip}   ${version}
#    run process         ssh-keygen -R ${pbc_ip}     shell=yes
#    # Check version before update
#    ${ver_before}       Get PBC Version  ${pbc_ip}
#    Log to Console      Version before update: ${ver_before}
#    Run Keyword if      '${ver_before}' == '${version}'         Log to Console  Already on ${version}
#    # Check if 2.0.2 or 2.1.0
#    ${contain202} =     Run Keyword And Return Status    Should Contain    ${ver_before}    2.0.2
#    ${contain210} =     Run Keyword And Return Status    Should Contain    ${ver_before}    2.1.0
#    # Kick off OTA
#    Run Keyword if      ${contain202} == True    202 Update     ${pbc_ip}
#    Run Keyword if      ${contain210} == True    210 Update     ${pbc_ip}   ${version}
#    # Check version after update
#    ${ver_after}        Get PBC Version  ${pbc_ip}
#    Log to Console      Version after update: ${ver_after}
#    # Compare versions to decide if update was successful
#    Run Keyword if      '${ver_after}' != '${ver_before}'      Log to Console  PASS
#    Run Keyword if      '${ver_after}' == '${ver_before}'      Log to Console  FAIL
#    Run Keyword if      '${ver_after}' == '${ver_before}'      FAIL            PBC did not update
#
#
#Get PBC Version
#    [Documentation]             Reads PBC Version
#    [Arguments]                 ${pbc_ip}
#    SSHLibrary.Open Connection  ${pbc_ip}              encoding=latin-1    timeout=15s
#    SSHLibrary.Login            rpe   password=rpe
#    ${ver} =                    SSHLibrary.Execute Command  cat /version.txt     return_stdout=True  return_stderr=False  return_rc=False  sudo=True  sudo_password=rpe
#    ${return_ver} =	            Get Substring	${ver}	12  -2
#    SSHLibrary.close all connections
#    [return]                    ${return_ver}
#
#202 Update
#    [Documentation]             Updates PBC linux if on version 2.0.2
#    [Arguments]                 ${pbc_ip}
#    SSHLibrary.Open Connection  ${pbc_ip}              encoding=latin-1    timeout=15s
#    SSHLibrary.Login            rpe   password=rpe
#    SSHLibrary.Execute Command  rm /version.txt        return_stdout=False  return_stderr=False  return_rc=False  sudo=True  sudo_password=rpe
##    SSHLibrary.Execute Command  /etc/rpe/update.sh     return_stdout=False  return_stderr=False  return_rc=False  sudo=True  sudo_password=rpe
#    SSHLibrary.write            /etc/rpe/update.sh
#    Log to Console      Updating...
#    Sleep               400s
#    SSHLibrary.close all connections
#
#
#210 Update
#    [Documentation]             Updates PBC linux if on version 2.1.0
#    [Arguments]                 ${pbc_ip}   ${version}
#    Telnet.Open Connection              ${pbc_ip}  port=2000
#    Telnet.Write                        SET_OTA=${version}
#    Log to Console      Updating...
#    Sleep               400s