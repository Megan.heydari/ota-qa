*** Settings ***
Documentation    Updating PBC Linux
Library  Process
Library  String
Library  SSHLibrary
Library  Telnet
Resource         /Users/megan.heydari/Desktop/ota-qa/OTA/Keywords/OTAKeywordLibrary.robot
Resource        /Users/megan.heydari/Desktop/ota-qa/OTA/Tests/OTAVariables.robot

*** Variables ***
# Versions
${ver_1}                  2.1.0-qa.4510
${ver_2}                  2.1.0-qa.4606



*** Test Cases ***
#PBC Linux OTA Cycle Test
#    [Documentation]  Update and downgrade PBC Linux OTA
#    FOR   ${var}  IN RANGE  100
#        Run Keyword if          ${var} == 0     Log to Console  \n
#        Log to Console          Attempt ${var}
#        Run Keyword And Warn On Failure         Linux OTA   ${pbc_ip}   ${ver_1}
#        Log to Console    \n
#        Run Keyword And Warn On Failure         Linux OTA   ${pbc_ip}   ${ver_2}
#        Log to console    \n
#    END

TEST
    Get Battery Operating Mode      192.168.1.41
#    Turn Load Off           192.168.1.41        R1_CHA
#    Check Energy Mode       192.168.1.41
#    Check Power Reading     192.168.1.41        R1_CHA
#
#
#
#    Turn Load On            192.168.1.41        R1_CHA
#    Check Energy Mode       192.168.1.41
#    Check Power Reading     192.168.1.41        R1_CHA
#
#
#
#    Turn Load Off           192.168.1.41        R1_CHA
#    Check Energy Mode       192.168.1.41
#    Check Power Reading     192.168.1.41        R1_CHA