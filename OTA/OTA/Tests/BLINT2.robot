*** Settings ***
Library  ./pythonHelpers/checkNordicStatus.py
Library   ./pythonHelpers/numCompanions.py
Library  SSHLibrary
Library  ImageHorizonLibrary
Library  ./pythonHelpers/ExtractGWVersion.py
Library  Process
Library  OperatingSystem



*** Variables ***
${pbc_ip}                   192.168.1.26
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${failures}                 0

*** Test Cases ***

Test case with loop assertion


    Establish ssh connection    ${pbc_ip}
#    SSHLibrary.Write  systemctl stop prudmxagent

    FOR   ${var}  IN RANGE  3

         Log to Console          /////////////////////////////////////////////
         Log to Console          Attempt ${var}



         ${V1}=                 Run Keyword And Continue On Failure     BL INT  ${pbc_ip}   ${nordic1}
         ${V2}=                 Run Keyword And Continue On Failure     BL INT  ${pbc_ip}   ${nordic2}
#         Log to Console  ${V1}
#         Log to Console  ${V2}
#         Run Keyword unless     '${V1}' == '0.0.0'    Counter     ${failures}
#         Run Keyword unless     '${V2}' == '0.0.0'    Counter     ${failures}
    END

    SSHLibrary.close all connections
#    Print Results   ${var}

*** Keywords ***
Establish ssh connection
    [Arguments]                         ${pbc}
    [timeout]                           10s
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1    timeout=200s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}


BL INT
    [Arguments]                         ${pbc}      ${MAC}
#    [timeout]                           5s
#    Establish ssh connection            ${pbc}
    SSHLibrary.Write                    systemctl stop prudmxagent
    Sleep                               5s
    Run keyword and ignore error        SSHLibrary.Write          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5
    sleep                               5s
#    ${stdout} =    Wait Until Keyword Succeeds	 20x	 1s       SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int    timeout=0.5s

    SSHLibrary.Write                    /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int
    sleep                               5s
    ${result} =                         SSHLibrary.Read Until         Done
    ${Version} =                        extractGWVersion    ${result}
    Log to Console                      ${MAC}: ${Version}
    [return]                            ${Version}
