# M. Heydari, RPE, OTA Automation testing 12/2/20

import sys
import base64

def selectAppVer(currApp, app1, app2):

    if currApp.find('0.1.19') != -1:
        return 'Companion-Gateway-Bootloader-Package-V0.1.21.zip'
    elif currApp.find('0.1.21') != -1:
        return 'Companion-Gateway-Bootloader-Package-V0.1.19.zip'
    else:
        return 'Companion-Gateway-Bootloader-Package-V0.1.19.zip'





if __name__ == "__main__":
    app1 = 'ver19'
    app2 = 'ver20'
    currapp = 'ver19'
    version = selectAppVer(currapp, app1, app2)
    print(version)
