# M. Heydari, RPE, OTA Automation testing 12/2/20

import sys
import base64

def numCompanions(input):

    startInd = input.find('Number of Companions: ') #22
    num = input[startInd+22:startInd+24]

    return num

def cmRepair(before, after):
    if before == after :
        return 0
    else:
        return 1




if __name__ == "__main__":
    with open('/Users/megan.heydari/Desktop/sampleOut.txt', 'r') as file:
        data = file.read().replace('\n', '')
    comp = numCompanions(data);
    print('Number of companions: ', comp)
