# M. Heydari, RPE, OTA Automation testing 10/13/20
import sys

def extractVersion(input):
    SET_OTA = input.find('SET_OTA')
    SET_OTA = SET_OTA + 8
    Version = input[SET_OTA:SET_OTA+7]
    print("Version:", Version)
    return Version


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("incorrect input")

    data = sys.argv[1]

    # with open('/Users/megan.heydari/Desktop/energyTest.txt', 'r') as file:
    #     data = file.read().replace('\n', '')
    # inFile = open('/Users/megan.heydari/Desktop/energyTest.txt')
    version = extractVersion(data)
    print(version)

