# M. Heydari, RPE, OTA Automation testing 10/12/20

import sys
import base64

def extractOTAProgress(input):
    SET_OTA = input.find('SET_OTA_PROGRESS')
    # SET_OTA = SET_OTA + 8
    Version = input[SET_OTA:]
    Version = Version.splitlines()[0]

    splitVersion = Version.split("=", 1)
    Progress = substring = splitVersion[1]

    progressDecode = base64.b64decode(Progress)
    Progress = progressDecode.decode('ascii')
    print("Progress:", Progress)
    return Progress

