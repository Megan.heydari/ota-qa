# M. Heydari, RPE, OTA Automation testing 10/28/20

import sys
import base64

def getAppVer(text, mac):
    verLines = []

    # new
    sub_str = mac + ' App'
    gwIndex = text.find(sub_str)
    Version1 = text[gwIndex +17:gwIndex+23]
    return  Version1

def getBLVer(text, mac):
    # new
    sub_str = mac + ' App'
    gwIndex = text.find(sub_str)
    Version1 = text[gwIndex + 27:gwIndex + 32]
    return Version1



if __name__ == "__main__":
    with open('/Users/megan.heydari/Desktop/sampleOut3.txt', 'r') as file:
        data = file.read().replace('\n', '')
    version = getBLVer(data, 'EE04BB8948C0')
    print(version)
