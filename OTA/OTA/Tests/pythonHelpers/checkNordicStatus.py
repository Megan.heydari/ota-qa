# M. Heydari, RPE, OTA Automation testing 11/18/20

def checkNordicStatus(input):
    V = input.find('Status=')
    start = V + 7
    Version = input[start:start+7]
    return Version

if __name__ == "__main__":
    with open('/Users/megan.heydari/Desktop/SampleINT.txt', 'r') as file:
        data = file.read().replace('\n', '')
    version = checkNordicStatus(data)
    print(version)

