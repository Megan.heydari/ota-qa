# M. Heydari, RPE, OTA Automation testing 11/12/20

def extractVersionFile(input):
    V = input.find('-V')
    start = V + 2
    zip = input.find('.zip')
    end = zip
    Version = input[start:end]
    return Version

if __name__ == "__main__":
    data = 'Companion-Gateway-Bootloader-Package-V0.1.19.zip'
    version = extractVersionFile(data)
    print(version)
