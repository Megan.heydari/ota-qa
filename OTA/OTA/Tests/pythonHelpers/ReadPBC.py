# M. Heydari, RPE, PCB Automation testing 09/22/20
# Out of date, don't use
import sys
import base64

# print(sys.argv[1][11:])
def removeMarker(input):
    return input[11:]

def decode(input):
    base64_message = input
    base64_bytes = input
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode('ascii')

    return message

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("incorrect input")

    input = sys.argv[1]
    input = removeMarker(input)
    decodedMessage = decode(input)
    print(decodedMessage)
