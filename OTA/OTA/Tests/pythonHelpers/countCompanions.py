# M. Heydari, RPE, OTA Automation testing 11/12/20

def CompanionNum(actual, desired):
    actual = actual.strip()
    actual = int(actual)
    if actual == desired:
        return 0
        # print('PASS')
    else:
        return 1
        # print('FAIL')

if __name__ == "__main__":
    data = 'Companion-Gateway-Bootloader-Package-V0.1.19.zip'
    version = CompanionNum('3', 3)
    print(version)