# M. Heydari, RPE, OTA Automation testing 10/28/20

import sys
import base64

def versionEqual(begin, end):
    if (begin == end) or (begin == 'None') or (end == 'None'):
        return 0
    else:
        return 1


if __name__ == "__main__":
    final = versionEqual('0.1.20', '0.1.20')
    print(final)
