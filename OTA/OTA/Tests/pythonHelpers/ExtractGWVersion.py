# M. Heydari, RPE, OTA Automation testing 10/28/20

import sys
import base64

def extractGWVersion(input):
    verLines = []

    # new
    gwIndex = input.find('GW App Version: ')
    Version1 = input[gwIndex:gwIndex+23]
    numIn = Version1.find('.')
    Version = Version1[numIn-1:]

    # verLines = input.split()
    #
    #
    # GWAppPrompt = 'GW App Version: '
    # Version = verLines[37]

    print("Version:", Version)
    return Version.strip()
    # return Version

if __name__ == "__main__":
    with open('/Users/megan.heydari/Desktop/sampleOut.txt', 'r') as file:
        data = file.read().replace('\n', '')
    version = extractGWVersion(data)
    print(version)
