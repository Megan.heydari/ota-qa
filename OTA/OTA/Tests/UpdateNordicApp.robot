*** Settings ***
Documentation    Updates the nordic app via OTA
Resource         /Users/megan.heydari/Desktop/ota-qa/OTA/Keywords/OTAKeywordLibrary.robot
Resource        /Users/megan.heydari/Desktop/ota-qa/OTA/Tests/OTAVariables.robot




*** Variables ***
${pbc_ip}                   192.168.1.24
#${nordic1}                  C8B1C66CBB9F      #MAC address, 1000
#${nordic2}                  E292564D4837      #MAC address, 1000
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${app_dir_remote}           /etc/rpe/FirmwareRelease/CompanionGateway
${app_local}                /Users/megan.heydari/Desktop/ota-qa/gwApp  #change the file path to reflect the file you want
${app19}                      Companion-Gateway-Bootloader-Package-V0.1.19.zip
${app20}                      Companion-Gateway-Bootloader-Package-V0.1.20.zip
${release_path}             /etc/rpe/FirmwareRelease/release.json
${local_release}            ./Releases/release.json
${HVAC}                     HVAC-Bootloader-Package-V0.0.7.zip


*** Test Cases ***

#Test
#    Get App Version PBC     ${nordic1}

Test 1
    FOR   ${var}  IN RANGE  100
        Run Keyword if          ${var} == 0     Log to Console  \n
        Log to Console          /////////////////////////////////////////////
#        ${att}=     ${var} + 1
        Log to Console          Attempt ${var}

        Run Keyword And Continue On Failure             Update App and Check Version    ${pbc_ip}

#        Run Keyword And Continue On Failure             App Signature Failure           ${pbc_ip}   ${HVAC}

#        Run Keyword And Continue On Failure             Update App and Check Version    ${pbc_ip}
#
#        Run Keyword And Continue On Failure             App Signature Failure           ${pbc_ip}   ${HVAC}

#        Run Keyword And Continue On Failure             Reset Nordic                    ${pbc_ip}   ${nordic1}
    END

#Test 2
#    Reset Nordic    ${pbc_ip}   ${nordic1}


*** Keywords ***
#Establish ssh connection
#    [Arguments]                         ${pbc}
#    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1    timeout=15s
#    SSHLibrary.Login with public key    ${username}         ${ssh_key}
#Move app file
##    Move local app file to correct location on PBC
#    [Arguments]                         ${local}      ${app}
#    ${local_path} =                     Catenate    SEPARATOR=/  ${local}   ${app}
#    SSHLibrary.Put File                 ${local_path}            /etc/rpe/FirmwareRelease/CompanionGateway  scp=ALL
#
#Modify release.json
##    Modify release.json to point to the new app
#    [Arguments]                         ${app}              ${release}          ${app_dir_remote}
#    ${new_version} =                    extractVersionFile                  ${app}
#    SSHLibrary.Get File                 ${release_path}     ${local_release}    scp=ALL
#    ${json_string}=    OperatingSystem.Get File     ${local_release}
#    ${json_obj}=    evaluate    json.loads('''${json_string}''')    json
#    ${remote_path}=  Catenate       SEPARATOR=/  ${app_dir_remote}   ${app}
#    set to dictionary    ${json_obj["gateway"]}   path=${remote_path}
#    set to dictionary    ${json_obj["gateway"]}   version=${new_version}
#    ${json_string}=    evaluate    json.dumps(${json_obj})    json
#    Create File    ./Releases/release.json    ${json_string}    UTF-8
#    SSHLibrary.Put File               ./Releases/release.json   /etc/rpe/FirmwareRelease    scp=ALL
#
#Trigger OTA
##    Trigger the OTA in telnet
#    [Arguments]                         ${pbc}
#
#
#    run process                         ssh root@${pbc_ip} systemctl restart prudmxagent   shell=yes
#    sleep                               10s
#    Telnet.Open Connection              ${pbc}  port=2000
##    SSHLibrary.Start Command            journalctl -f
##    sleep                               60s
##    Log to Console                      Triggering OTA
#    Telnet.Write                        @gateway SET_OTA=X.X.X
#    sleep                               400s
#
#
#Get App Version PBC (orig)
##    Confirm version number has changed using cgw-testtool
#
#    [Arguments]                         ${MAC}
#    [Timeout]                           120s
#    run process                         ssh root@${pbc_ip} systemctl stop prudmxagent   shell=yes
#    sleep                               5s
##    ${result} =                         Wait Until Keyword Succeeds  20s  1.5s         run process          ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int  shell=yes  #timeout=1.5s  on_timeout=kill
#    ${result} =                         run process          ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int  shell=yes  #timeout=1.5s  on_timeout=kill
#
#    ${Version} =                        extractGWVersion    ${result.stdout}
#
#    [return]                            ${Version}
#
#Get App Version PBC
##    Confirm version number has changed using cgw-testtool
#
#    [Arguments]                         ${MAC}
#    [Timeout]                           120s
#    Establish ssh connection            ${pbc_ip}
#    run process                         ssh root@${pbc_ip} systemctl restart prudmxagent   shell=yes
#    SSHLibrary.write                    journalctl -f
#    ${out} =                            Run keyword and continue on failure        SSHLibrary.read until regexp         Will start reading energy every 1.0s...
#    ${Version} =                        getAppVer    ${out}   ${MAC}
#
#    [return]                            ${Version}
#
#
#Update App and Check Version
#    [Arguments]                ${pbc_ip}
#
#    #SSH into PBC
#    Establish ssh connection    ${pbc_ip}
##    SSHLibrary.Execute Command  reboot
##    Sleep                       60s
#
#    #UPDATE APP
#    Establish ssh connection    ${pbc_ip}
#    #Copy desired gw app file to pbc
#
##    ${version1_before}  ${rc0}=  Get App Version PBC         ${nordic1}
##    ${version2_before}  ${rc1}=  Get App Version PBC         ${nordic2}
#
#    ${version1_before} =  Run Keyword and continue on failure   Get App Version PBC         ${nordic1}
#    ${version2_before} =  Run Keyword and continue on failure   Get App Version PBC         ${nordic2}
#
#
##    Log to Console              Version Before 0: ${version1_before}
##    Log to Console              Version Before 1: ${version2_before}
#
#
#    ${app}=                     selectAppVer              ${version1_before}    0.1.19      0.1.20
##    Log to Console              ${version1_before}
##    Log to console              ${app}
##    Log to Console              SELECTED APP ${app}
#    Move app file               ${app_local}        ${app}
#    #Modify release.json to match your app
#    Modify release.json         ${app}              ${release_path}          ${app_dir_remote}
#
##    Log to Console              Triggering the OTA
#    Trigger OTA                 ${pbc_ip}
#
#    #REBOOT
##    Establish ssh connection    ${pbc_ip}
##
##    SSHLibrary.Execute Command  reboot
##    Sleep                       60s
#
#    #CHECK VERSION
##    Establish ssh connection    ${pbc_ip}
##    ${version1_after}  ${rc0_after}  ${version2_after}  ${rc1_after}=     Redo Restart
#
##    Run Keyword if              ${version1_after}==${version1_before} or ${version2_after}==${version2_before}
#
#    ${version1_after} =  Run Keyword And Continue On Failure     Get App Version PBC         ${nordic1}
#    ${version2_after} =  Run Keyword And Continue On Failure     Get App Version PBC         ${nordic2}
##    Log to console                     Version After 0: ${version1_after}
#
##    Log to console                     Version After 1: ${version2_after}
##    Log to Console                     Return Code After 0: ${rc0_after}
##    Log to Console                     Return Code After 1: ${rc1_after}
#
#    Log                                Version After 0: ${version1_after}
#    Log                                Version After 1: ${version2_after}
#    SSHLibrary.close all connections
#
#    Log to Console      Update:
#    UpdateSuccess       ${version1_before}   ${version1_after}
#    UpdateSuccess       ${version2_before}   ${version2_after}
#
#UpdateSuccess
#    [Arguments]     ${before}  ${after}
#    ${res} =        versionEqual    ${before}   ${after}
#    Run Keyword if        ${res} == 1           Log to Console      PASS
#    Run Keyword if	      ${res} == 0       	Log to Console      FAIL
#
#
#
#
#### CHECK SIG ##################################################################
#App Signature Failure
#    [Arguments]                         ${pbc_ip}   ${app}
##    Reboot PBC                          ${pbc_ip}
#    Establish ssh connection    ${pbc_ip}
#
#    Move app file               ${app_local}        ${app}
#
#    Modify release.json         ${app}              ${release_path}          ${app_dir_remote}
#
#    ${version1_before}=  Get App Version PBC         ${nordic1}
#    ${version2_before}=  Get App Version PBC         ${nordic2}
#    Trigger OTA Intentional Fail                 ${pbc_ip}
#    ${version1_after}=  Get App Version PBC         ${nordic1}
#    ${version2_after}=  Get App Version PBC         ${nordic2}
#
#    Log to Console        Check App Sig:
#    PassFail              ${version1_after}   ${version1_before}
#    PassFail              ${version2_after}   ${version2_before}
#
#
#PassFail
#    [Arguments]           ${after}  ${before}
#    ${res} =              versionEqual          ${after}  ${before}
#    Run Keyword if	      ${res} == 1       	Log to Console      FAIL
#    Run Keyword if        ${res} == 0           Log to Console      PASS
#
#
#Trigger OTA Intentional Fail
##    Trigger the OTA in telnet
#    [Arguments]                         ${pbc}
#    Telnet.Open Connection              ${pbc}  port=2000
#    SSHLibrary.Execute Command          systemctl restart prudmxagent
#    SSHLibrary.Start Command            journalctl -f
#    sleep                               60s
#    Telnet.Write                        @gateway SET_OTA=X.X.X
#    sleep                               60s
#
#
## RESET
#Reset Nordic
#    [Arguments]         ${pbc_ip}   ${MAC}
#    ${result_before} =         Wait Until Keyword Succeeds  45s  1s    run process     ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int     shell=yes
#    ${cm_before} =      numCompanions   ${result_before.stdout}
#
#    Run keyword and ignore error  run process         ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} reset  shell=yes    timeout=1s  on_timeout=kill
#    sleep   10s
#    run process         ssh root@${pbc_ip} systemctl restart prudmxagent    shell=yes   #should connect automatically, 20s
#    sleep   160s
#    run process         ssh root@${pbc_ip} systemctl stop prudmxagent    shell=yes
#    sleep   2s
#    ${result_after} =         Wait Until Keyword Succeeds  45s  1s    run process     ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int     shell=yes
#    ${cm_after} =      numCompanions   ${result_after.stdout}
#
#    ${cmres} =            cmRepair    ${cm_before}    ${cm_after}
#    Log to Console      Nordic Reset
#    Run Keyword if      ${cmres} == 0       Log to Console      PASS
#    Run Keyword if      ${cmres} == 1       Log to Console      FAIL
