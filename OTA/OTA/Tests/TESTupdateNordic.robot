*** Settings ***
Documentation    Suite description
Resource         /Users/megan.heydari/Desktop/ota-qa/OTA/Keywords/OTAKeywordLibrary.robot
Resource        /Users/megan.heydari/Desktop/ota-qa/OTA/Tests/OTAVariables.robot

*** Variables ***
## PBC Info
#${pbc_ip}                   192.168.1.24
#${nordic1}                  EE04BB8948C0      #MAC address, PoE
#${nordic2}                  E833177C544A      #MAC address, PoE
## SSH Info
#${username}                 root
#${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
#${app_dir_remote}           /etc/rpe/FirmwareRelease/CompanionGateway
## App file directory info
#${app_local}                /Users/megan.heydari/Desktop/ota-qa/gwApp  #change the file path to reflect the file you want
#${app19}                    Companion-Gateway-Bootloader-Package-V0.1.19.zip
#${app20}                    Companion-Gateway-Bootloader-Package-V0.1.20.zip
#${release_path}             /etc/rpe/FirmwareRelease/release.json
#${local_release}            ./Releases/release.json
#${HVAC}                     HVAC-Bootloader-Package-V0.0.7.zip

*** Test Cases ***
Test 1
    Establish ssh connection    ${pbc_ip}
    Reboot PBC                  ${pbc_ip}

#Test 1
#    FOR   ${var}  IN RANGE  2
#        Run Keyword if          ${var} == 0     Log to Console  \n
#        Log to Console          /////////////////////////////////////////////
#        Log to Console          Attempt ${var}
#
#        Run Keyword And Continue On Failure             Update App and Check Version    ${pbc_ip}
#
#    END

*** Keywords ***
