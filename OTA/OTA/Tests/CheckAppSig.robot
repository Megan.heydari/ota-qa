*** Settings ***
Documentation    Suite description
Library  Telnet
Library  Process
Library  SCPLibrary
Library  SSHLibrary
Library  String
Library  OperatingSystem
Library  ./pythonHelpers/ExtractGWVersion.py
Library  ./pythonHelpers/ExtractVersionFileName.py
Library   Collections
Library  ./pythonHelpers/versionEqual.py


*** Variables ***
${pbc_ip}                   192.168.1.44
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${app_dir_remote}           /etc/rpe/FirmwareRelease/CompanionGateway
${app_local}                /Users/megan.heydari/Desktop/ota-qa/HVAC  #change the file path to reflect the file you want
${app19}                    HVAC-Bootloader-Package-V0.0.7.zip
${app20}                    HVAC-Bootloader-Package-V0.0.8.zip
${release_path}             /etc/rpe/FirmwareRelease/release.json
${local_release}            ./Releases/release.json


*** Test Cases ***
Signature Check
    FOR   ${var}  IN RANGE  10
        Log to Console          \n
        Log to Console          /////////////////////////////////////////////
        Log to Console          Attempt ${var}
        Run Keyword And Continue On Failure         App Signature Failure   ${pbc_ip}   ${app19}

    END


*** Keywords ***
Establish ssh connection
    [Arguments]                         ${pbc_ip}
    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1     timeout= 45 s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}


Get App Version PBC
#    Confirm version number has changed using cgw-testtool
    [Arguments]                         ${MAC}
    SSHLibrary.Execute Command          systemctl stop prudmxagent
    ${result} =                        Wait Until Keyword Succeeds  45s  1s         run process          ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int  shell=yes  #timeout=1.5s  on_timeout=kill
#    sleep                               15s
#    ${result} =                         Wait Until Keyword Succeeds  30x    2 sec   SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int   return_stdout=True   timeout=1s
    ${Version} =                        extractGWVersion    ${result.stdout}
    [return]                            ${Version}

Reboot PBC
    [Arguments]                 ${pbc_ip}
#    Log to Console              Rebooting PBC . . .
    Establish ssh connection    ${pbc_ip}
#    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
    SSHLibrary.Execute Command  reboot
    Sleep                       60s

Move app file
#    Move local app file to correct location on PBC
    [Arguments]                         ${local}            ${remote}       ${app}
    ${local_path} =                     Catenate    SEPARATOR=/  ${local}   ${app}
    SSHLibrary.Put File                 ${local_path}            ${remote}  scp=ALL

Modify release.json
#    Modify release.json to point to the new app
    [Arguments]                         ${app}              ${release}          ${app_dir_remote}
    ${new_version} =                    extractVersionFile                  ${app}
    SSHLibrary.Get File                 ${release_path}     ${local_release}    scp=ALL
    ${json_string}=    OperatingSystem.Get File     ${local_release}
    ${json_obj}=    evaluate    json.loads('''${json_string}''')    json
    ${remote_path}=  Catenate       SEPARATOR=/  ${app_dir_remote}   ${app}
    set to dictionary    ${json_obj["gateway"]}   path=${remote_path}
    set to dictionary    ${json_obj["gateway"]}   version=${new_version}
    ${json_string}=    evaluate    json.dumps(${json_obj})    json
#    log to console       \nNew JSON string:\n${json_string}
    Create File    ./Releases/release.json    ${json_string}    UTF-8
    SSHLibrary.Put File               ./Releases/release.json   /etc/rpe/FirmwareRelease    scp=ALL

Trigger OTA
#    Trigger the OTA in telnet
    [Arguments]                         ${pbc}
#    [Timeout]                           200s
    Telnet.Open Connection              ${pbc}  port=2000
    SSHLibrary.Execute Command          systemctl restart prudmxagent
    sleep                               30s
    Telnet.Write                        @gateway SET_OTA=X.X.X
    SSHLibrary.Start Command            journalctl -f
#    ${output}=                          SSHLibrary.Read Until               Warning: Companion gateway update attempt complete.

#    SSHLibrary.Read Until               except ValidationException:
    sleep                               60s
#    Should Contain	                    ${output}	                        except ValidationException:
#    Log to Console                      Rejected HVAC App


App Signature Failure
    [Arguments]                         ${pbc_ip}   ${app}
    Reboot PBC                          ${pbc_ip}
    Establish ssh connection    ${pbc_ip}

    #Copy desired gw app file to pbc
########################################################################## Logs
#    Log to console              Moving app to pbc
########################################################################## Logs

    Move app file               ${app_local}        ${app_dir_remote}       ${app}

    #Modify release.json to match your app
########################################################################## Logs
#    Log to console              Modifying release.json to match app
########################################################################## Logs


    Modify release.json         ${app}              ${release_path}          ${app_dir_remote}
#    run process                 ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${nordic1} int  shell=yes
#    run process                 ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${nordic2} int  shell=yes

    ${version1_before}=  Get App Version PBC         ${nordic1}
    ${version2_before}=  Get App Version PBC         ${nordic2}

########################################################################## Logs
#    Log to Console              Version Before 0: ${version1_before}\n
#    Log to Console              Version Before 1: ${version2_before}\n
########################################################################## Logs


#    Log to Console              Triggering the OTA
    Trigger OTA                 ${pbc_ip}
#    Log to Console              Checking version after
    ${version1_after}=  Get App Version PBC         ${nordic1}
    ${version2_after}=  Get App Version PBC         ${nordic2}

########################################################################## Logs
#    Log to Console              Version Before 0: ${version1_after}\n
#    Log to Console              Version Before 1: ${version2_after}\n
########################################################################## Logs

    PassFail              ${version1_after}   ${version1_before}
    PassFail              ${version2_after}   ${version2_before}

#    PassFail              0.1.19    0.1.20


PassFail
    [Arguments]           ${after}  ${before}
    ${res} =              versionEqual          ${after}  ${before}
    Run Keyword if	      ${res} == 1       	Log to Console      FAIL
    Run Keyword if        ${res} == 0           Log to Console      PASS



