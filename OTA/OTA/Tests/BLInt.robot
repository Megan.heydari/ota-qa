*** Settings ***
Library  ./pythonHelpers/checkNordicStatus.py
Library   ./pythonHelpers/numCompanions.py
Library  SSHLibrary
Library  ImageHorizonLibrary
Library  ./pythonHelpers/ExtractGWVersion.py
Library  Process
Library  OperatingSystem



*** Variables ***
${pbc_ip}                   192.168.1.24
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${failures}                 0

*** Test Cases ***

Test case with loop assertion

#    Establish ssh connection    ${pbc_ip}
#    Sleep                       10s


    FOR   ${var}  IN RANGE  3
         Establish ssh connection    ${pbc_ip}
         SSHLibrary.Write  systemctl stop prudmxagent
         Log to Console          /////////////////////////////////////////////
         Log to Console          Attempt ${var}

#         Reboot PBC                  ${pbc_ip}
#         Establish ssh connection    ${pbc_ip}
#         SSHLibrary.Execute Command  systemctl stop prudmxagent

         ${V1}=                 Run Keyword And Continue On Failure     BL INT  ${pbc_ip}   ${nordic1}
         ${V2}=                 Run Keyword And Continue On Failure     BL INT  ${pbc_ip}   ${nordic2}
#         Log to Console  ${V1}
#         Log to Console  ${V2}
#         Run Keyword unless     '${V1}' == '0.0.0'    Counter     ${failures}
#         Run Keyword unless     '${V2}' == '0.0.0'    Counter     ${failures}
         SSHLibrary.close all connections
    END


#    Print Results   ${var}


*** Keywords ***

Establish ssh connection
    [Arguments]                         ${pbc}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1    timeout=3s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}

BL INT
    [Arguments]                        ${MAC}   ${pbc_ip}
    Run Keyword And Ignore Error    SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5  timeout=5s
    sleep                     3s
    ${result} = SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int   return_stdout=True   timeout=2s
    ${Version} =                extractGWVersion    ${result}
    Log to Console              ${MAC}: ${Version}
    [Return]                    ${Version}

Enter BL
   [Arguments]                         ${MAC}  ${pbc_ip}
   Run Keyword And Ignore Error         SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5    timeout=5s
#   SSHLibrary.Write          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5 & pid=$!; sleep 1; kill "$pid"
   sleep                     1s
#   SSHLibrary.Write          kill "$pid"


Get App Version PBC
#    Confirm version number has changed using cgw-testtool
    [Arguments]                         ${MAC}
#    sleep                               15s
    ${result} =                         Wait Until Keyword Succeeds  1 minute    2s   SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int   return_stdout=True   timeout=2s
#    SSHLibrary.Write                    /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int & pid=$! ; sleep 1 ; kill "$pid"
#    ${result} =   SSHLibrary.Write until expected output         /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int & pid=$! ; sleep 1 ; kill "$pid"    expected=Done   timeout=10s	 retry_interval=1.5s


#    ${result} =                         SSHLibrary.Read Until  Done
    ${Version} =                        extractGWVersion    ${result}
    [return]                            ${Version}

#Get App Version PBC
#    [Arguments]                         ${MAC}
#    [Timeout]                           20s
#    run process                         ssh root@${pbc_ip} systemctl stop prudmxagent   shell=yes
#    sleep                               5s
#    ${result} =                         run process          ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int  shell=yes  #timeout=1.5s  on_timeout=kill
#    ${Version} =                        extractGWVersion    ${result.stdout}
#    [return]                            ${Version}

Reboot PBC
    [Arguments]                 ${pbc_ip}
    #SSH into PBC
#    Log to Console              Establish ssh connection
    Establish ssh connection    ${pbc_ip}
#    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
    SSHLibrary.Execute Command  reboot
    Sleep                       60s
    SSHLibrary.close all connections


#BL INT
#    [Arguments]                 ${pbc_ip}   ${MAC}
#    [Timeout]                   90 s
#
##    Establish ssh connection    ${pbc_ip}
##    SSHLibrary.Write  systemctl stop prudmxagent
##    Establish ssh connection    ${pbc_ip}
#    Enter BL                    ${MAC}  ${pbc_ip}
#
#    ${Version}=                 Get App Version PBC         ${MAC}
#    Log to Console              ${MAC}: ${Version}
##    SSHLibrary.close all connections
#
#    [Return]                    ${Version}

Counter
    [Arguments]                 ${in}
    ${in} =                     Evaluate  ${in}+1

Print Results
    [Arguments]                 ${var}
    ${finalVar} =               Evaluate    ${var} + ${var} + 1
    Log to Console          Out of ${finalVar} cases, ${failures} cases failed

Run INT

