*** Settings ***
Library  ./pythonHelpers/checkNordicStatus.py
Library   ./pythonHelpers/numCompanions.py
Library  SSHLibrary
Library  ImageHorizonLibrary
Library   ./pythonHelpers/countCompanions.py


*** Variables ***
${pbc_ip}                   192.168.1.19
${nordic1}                  EE04BB8948C0      #MAC address, PoE
${nordic2}                  E833177C544A      #MAC address, PoE
${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa
${app_dir_remote}           /etc/rpe/FirmwareRelease/CompanionGateway
${app_local}                /Users/megan.heydari/Desktop/ota-qa/gwApp  #change the file path to reflect the file you want
#${app}                      Companion-Gateway-Bootloader-Package-V0.1.19.zip
#${app}                      Companion-Gateway-Bootloader-Package-V0.1.20.zip
${release_path}             /etc/rpe/FirmwareRelease/release.json
${local_release}            ./Releases/release.json

*** Test Cases ***
#Reboot
#    Reboot PBC              ${pbc_ip}
Reset Test Cycle
    FOR   ${var}  IN RANGE  100
        Log to Console          /////////////////////////////////////////////
        Log to Console          Attempt ${var}

        Reboot PBC              ${pbc_ip}

        Run Keyword And Continue On Failure             Perform Reset   ${nordic1}  ${pbc_ip}
#        Log to Console          Nordic1 Status: ${s1}
#        Log to Console          Nordic1 Companions: ${c1}

#        Run Keyword And Continue On Failure             Perform Reset   ${nordic2}  ${pbc_ip}
##        Log to Console          Nordic1 Status: ${s2}
##        Log to Console          Nordic1 Companions: ${c2}

    END

*** Keywords ***
Establish ssh connection
    [Arguments]                         ${pbc}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1
    SSHLibrary.Login with public key    ${username}         ${ssh_key}

Reset Nordic
    [Arguments]                         ${uid}
    Write                               /home/rpe/cgw-testtool /dev/ttyS2 ${uid} reset
    sleep                               3s
    Close All Connections


Check Status
    [Arguments]                         ${uid}
    ${result} =                         Wait Until Keyword Succeeds  1 minute    30ms   Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${uid} int   return_stdout=True  timeout=1s
#    Log to Console                      ${result}
    ${status}=                          checkNordicStatus                   ${result}
    ${companions}=                      numCompanions                       ${result}
#    Log to console                      ${status}
#    Log to console                      ${companions}
    [return]                            ${status}   ${companions}

Reboot PBC
    [Arguments]                 ${pbc_ip}
    #SSH into PBC
#    Log to Console              Establish ssh connection
    Establish ssh connection    ${pbc_ip}
#    SSHLibrary.Open Connection          ${pbc_ip}              encoding=latin-1
    SSHLibrary.Execute Command  reboot
    Sleep                       80s

Perform Reset
#    Reboot PBC                          ${pbc_ip}
    [Arguments]                         ${nordic}   ${pbc_ip}
    Establish ssh connection            ${pbc_ip}
    Sleep                               3s
    Reset Nordic                        ${nordic}
    Sleep                               15s
    Establish ssh connection            ${pbc_ip}
#    Sleep                               10s
    ${nordicStatus}    ${companions}=  Check Status                        ${nordic}
#    Log to Console                      ${nordic} Status: ${nordicStatus}
#    Log to Console                      ${nordic} Companions: ${companions}

#    ${res} =                            CompanionNuum       ${companions}   3
    run keyword if                      ${companions} == 3         Log to Console  PASS
    run keyword if                      ${companions} != 3         Log to Console  FAIL


#    [Return]                            ${nordicStatus}    ${companions}