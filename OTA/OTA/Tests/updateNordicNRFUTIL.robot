*** Settings ***
Library  Telnet
Library  Process
Library  SCPLibrary
Library  SSHLibrary
Library  String
Library  OperatingSystem
Library  ./pythonHelpers/ExtractGWVersion.py
Library   Collections
Library  ./pythonHelpers/versionEqual.py
Library  ./pythonHelpers/BLAppVer.py


*** Variables ***
${pbc_ip}                   192.168.1.24      #PBC IP address           (1)
${nordic1}                  EE04BB8948C0      #MAC address, PoE         (2)
${nordic2}                  E833177C544A      #MAC address, PoE         (3)

#${pbc_ip}                   192.168.1.23
#${nordic1}                  C8B1C66CBB9F      #MAC address, 1000
#${nordic2}                  E292564D4837      #MAC address, 1000

${username}                 root
${ssh_key}                  /Users/megan.heydari/.ssh/id_rsa    #replace with your ssh key          (4)
${app19}                    Companion-Gateway-Bootloader-Package-V0.1.19.zip  #SCP this file to pbc to /etc/rpe/FirmwareRelease/CompanionGateway/       (5)
${app20}                    Companion-Gateway-Bootloader-Package-V0.1.20.zip  #SCP this file to pbc to /etc/rpe/FirmwareRelease/CompanionGateway/       (6)


*** Test Cases ***
Test 1

    FOR   ${var}  IN RANGE  150
            Run Keyword if          ${var} == 0     Log to Console  \n
            Log to Console          /////////////////////////////////////////////
            Log to Console          Attempt ${var}

            Run Keyword And Continue On Failure             Trigger Update      ${app20}       ${nordic1}
            Run Keyword And Continue On Failure             Trigger Update      ${app20}       ${nordic2}

            Run Keyword And Continue On Failure             Trigger Update      ${app19}       ${nordic1}
            Run Keyword And Continue On Failure             Trigger Update      ${app19}       ${nordic2}

            END

#Test
#    Establish ssh connection    ${pbc_ip}
#
#
#
#    SSHLibrary.start command    echo Hello World
##    ${out}=  SSHLibrary.read
#
#    ${out}=  SSHLibrary.read command output






*** Keywords ***
Establish ssh connection
    [Arguments]                         ${pbc}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1    timeout=200s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}

Trigger Update
    [Arguments]         ${Version}   ${mac}
    Establish ssh connection    ${pbc_ip}
    run process          ssh root@${pbc_ip} systemctl stop prudmxagent   shell=yes
    ${N1} =              catenate   SEPARATOR=       sudo nrfutil -v -v dfu serial -p /dev/ttyS2 -d B -pkg /etc/rpe/FirmwareRelease/CompanionGateway/  ${Version}
    ${N2} =              catenate   SEPARATOR=       sudo nrfutil -v -v dfu serial -p /dev/ttyS2 -d A -pkg /etc/rpe/FirmwareRelease/CompanionGateway/  ${Version}
#    Run Keyword if       "${mac}" == "${nordic1}"       SSHLibrary.write     ${N1}
#    Run Keyword if       "${mac}" == "${nordic2}"       SSHLibrary.write     ${N2}
    Run Keyword if       "${mac}" == "${nordic1}"       SSHLibrary.write     ${N1}
    Run Keyword if       "${mac}" == "${nordic2}"       SSHLibrary.write     ${N2}
    ${out} =             SSHLibrary.Read
    Sleep                200s
    SSHLibrary.close all connections
    ${version1_after}=          Run keyword and continue on failure  Get Version     ${mac}
    Log to Console             ${mac}: ${version1_after}
    Run Keyword if       "${Version}" == "${app19}"     UpdateSuccess       0.1.19   ${version1_after}
    Run Keyword if       "${Version}" == "${app20}"     UpdateSuccess       0.1.20   ${version1_after}

#Get Version
#    [Arguments]                         ${MAC}
#    [Timeout]                           120s
#    run process                         ssh root@${pbc_ip} systemctl stop prudmxagent   shell=yes
#    sleep                               5s
#    ${result} =                         run process          ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int  shell=yes  #timeout=1.5s  on_timeout=kill
#    ${Version} =                        extractGWVersion    ${result.stdout}
#    [return]                            ${Version}

Get Version
#    Confirm version number has changed using cgw-testtool

    [Arguments]                         ${MAC}
    [Timeout]                           120s
    Establish ssh connection            ${pbc_ip}
    run process                         ssh root@${pbc_ip} systemctl restart prudmxagent   shell=yes
    SSHLibrary.write                    journalctl -f
    ${out} =                            Run keyword and continue on failure        SSHLibrary.read until regexp         Will start reading energy every 1.0s...
    ${Version}=                        getAppVer    ${out}   ${MAC}

    [return]                            ${Version}


UpdateSuccess
    [Arguments]     ${before}  ${after}
    ${res} =        versionEqual    ${before}   ${after}
    Run Keyword if        ${res} == 1           Log to Console      FAIL
    Run Keyword if	      ${res} == 0       	Log to Console      PASS



