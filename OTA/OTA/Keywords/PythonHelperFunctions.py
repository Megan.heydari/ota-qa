#BL App Verification
def getAppVer(text, mac):
    verLines = []

    # new
    sub_str = mac + ' App'
    gwIndex = text.find(sub_str)
    Version1 = text[gwIndex +17:gwIndex+23]
    return  Version1

def getBLVer(text, mac):
    # new
    sub_str = mac + ' App'
    gwIndex = text.find(sub_str)
    Version1 = text[gwIndex + 27:gwIndex + 32]
    return Version1

#Check if Nordics are online
def checkNordicStatus(input):
    V = input.find('Status=')
    start = V + 7
    Version = input[start:start+7]
    return Version


# Count paired companions
def CompanionNum(actual, desired):
    actual = actual.strip()
    actual = int(actual)
    if actual == desired:
        return 0
        # print('PASS')
    else:
        return 1
        # print('FAIL')


# Count passing cases
def countPass(input):
    test_sub = 'PASS'
    res = [i for i in range(len(input)) if input.startswith(test_sub, i)]

    return len(res)

# Check if nordics connected
def didNordicsConnect(input):
    if bool(input) == False:
        return 1
    elif input.find('Discovered nordics:') != -1 or input.find('Found nordics:') != -1:
        return 0
    else:
        return 1


# Extract gw version
def extractGWVersion(input):
    gwIndex = input.find('GW App Version: ')
    Version1 = input[gwIndex:gwIndex+23]
    numIn = Version1.find('.')
    Version = Version1[numIn-1:]
    print("Version:", Version)
    return Version.strip()


def extractOTAProgress(input):
    SET_OTA = input.find('SET_OTA_PROGRESS')
    # SET_OTA = SET_OTA + 8
    Version = input[SET_OTA:]
    Version = Version.splitlines()[0]

    splitVersion = Version.split("=", 1)
    Progress = substring = splitVersion[1]

    progressDecode = base64.b64decode(Progress)
    Progress = progressDecode.decode('ascii')
    print("Progress:", Progress)
    return Progress

def extractVersion(input):
    SET_OTA = input.find('SET_OTA')
    SET_OTA = SET_OTA + 8
    Version = input[SET_OTA:SET_OTA+7]
    print("Version:", Version)
    return Version

def extractVersionFile(input):
    V = input.find('-V')
    start = V + 2
    zip = input.find('.zip')
    end = zip
    Version = input[start:end]
    return Version

def numCompanions(input):
    if bool(input) == False:
        return -1
    else:
        startInd = input.find('Number of Companions: ') #22
        num = input[startInd+22:startInd+24]
        return num

def cmRepair(before, after):
    if before == after :
        return 0
    else:
        return 1

def removeMarker(input):
    return input[11:]

def decode(input):
    base64_message = input
    base64_bytes = input
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode('ascii')

    return message

def selectAppVer(currApp, app1, app2):

    if currApp.find('0.1.19') != -1:
        return 'Companion-Gateway-Bootloader-Package-V0.1.21.zip'
    elif currApp.find('0.1.21') != -1:
        return 'Companion-Gateway-Bootloader-Package-V0.1.19.zip'
    else:
        return 'Companion-Gateway-Bootloader-Package-V0.1.19.zip'

def versionEqual(begin, end):
    if (begin == end) or (begin == 'None') or (end == 'None'):
        return 0
    else:
        return 1


def RebootEvaluateTest(before, after):
    if (before == after) and (before != -1) and (after != -1):
        return 0
    else:
        return 1
