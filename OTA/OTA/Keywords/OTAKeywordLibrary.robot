*** Settings ***
Library  Telnet
Library  Process
Library  SCPLibrary
Library  SSHLibrary
Library  String
Library  OperatingSystem
Library   Collections
Library  ./PythonHelperFunctions.py


*** Keywords ***
Establish ssh connection
    [Documentation]                     Establishes ssh connection with the specified IP address
    [Arguments]                         ${pbc}
    SSHLibrary.Open Connection          ${pbc}              encoding=latin-1    timeout=15s
    SSHLibrary.Login with public key    ${username}         ${ssh_key}

Move app file
    [Documentation]                     Move local app file to correct location on PBC to the CompanionGateway Directory
    [Arguments]                         ${local}       ${app}
    ${local_path} =                     Catenate    SEPARATOR=/  ${local}   ${app}
    SSHLibrary.Put File                 ${local_path}            /etc/rpe/FirmwareRelease/CompanionGateway  scp=ALL

Modify release.json
    [Documentation]                     Modifies the release.json file on the PBC to specify the path of the file chosen for OTA
    [Arguments]                         ${app}              ${release}          ${app_dir_remote}
    ${new_version} =                    extractVersionFile                  ${app}
    SSHLibrary.Get File                 ${release_path}     ${local_release}    scp=ALL
    ${json_string}=    OperatingSystem.Get File     ${local_release}
    ${json_obj}=    evaluate    json.loads('''${json_string}''')    json
    ${remote_path}=  Catenate       SEPARATOR=/  ${app_dir_remote}   ${app}
    set to dictionary    ${json_obj["gateway"]}   path=${remote_path}
    set to dictionary    ${json_obj["gateway"]}   version=${new_version}
    ${json_string}=    evaluate    json.dumps(${json_obj})    json
    Create File    ./Releases/release.json    ${json_string}    UTF-8
    SSHLibrary.Put File               ./Releases/release.json   /etc/rpe/FirmwareRelease    scp=ALL

Trigger OTA
    [Documentation]                     Send telnet command to trigger the gateway OTA. Waits 400s for the OTA to complete.
    [Arguments]                         ${pbc}
    run process                         ssh root@${pbc_ip} systemctl restart prudmxagent   shell=yes
    sleep                               10s
    Telnet.Open Connection              ${pbc}  port=2000
    Telnet.Write                        @gateway SET_OTA=X.X.X
    sleep                               400s


Get App Version PBC Original
    [Documentation]                     Confirm version number has changed using cgw-testtool. Doesn't work well
    [Arguments]                         ${MAC}
    [Timeout]                           120s
    run process                         ssh root@${pbc_ip} systemctl stop prudmxagent   shell=yes
    sleep                               5s
    ${result} =                         run process          ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int  shell=yes  #timeout=1.5s  on_timeout=kill
    ${Version} =                        extractGWVersion    ${result.stdout}
    [return]                            ${Version}

Get App Version PBC
    [Documentation]                     Checks the gw app version by restarting prudmxagent and reading the journalctl logs
    [Arguments]                         ${MAC}
    [Timeout]                           120s
    Establish ssh connection            ${pbc_ip}
    run process                         ssh root@${pbc_ip} systemctl restart prudmxagent   shell=yes
    SSHLibrary.write                    journalctl -f
    ${out} =                            Run keyword and continue on failure        SSHLibrary.read until regexp         Will start reading energy every 1.0s...
    ${Version} =                        getAppVer    ${out}   ${MAC}
    [return]                            ${Version}


Update App and Check Version
    [Documentation]            Kicks off the update and checks the gw app version
    [Arguments]                ${pbc_ip}
    #SSH into PBC
#    Establish ssh connection    ${pbc_ip}
#    SSHLibrary.Execute Command  reboot
#    Sleep                       60s
    #UPDATE APP
    Establish ssh connection    ${pbc_ip}
    ${version1_before} =  Run Keyword and continue on failure   Get App Version PBC         ${nordic1}
    ${version2_before} =  Run Keyword and continue on failure   Get App Version PBC         ${nordic2}
    ${app}=                     selectAppVer              ${version1_before}    0.1.19      0.1.20
    Move app file               ${app_local}        ${app}
    #Modify release.json to match your app
    Modify release.json         ${app}              ${release_path}          ${app_dir_remote}
    Trigger OTA                 ${pbc_ip}
    ${version1_after} =  Run Keyword And Continue On Failure     Get App Version PBC         ${nordic1}
    ${version2_after} =  Run Keyword And Continue On Failure     Get App Version PBC         ${nordic2}
    Log                                Version After 0: ${version1_after}
    Log                                Version After 1: ${version2_after}
    SSHLibrary.close all connections
    Log to Console      Update:
    UpdateSuccess       ${version1_before}   ${version1_after}
    UpdateSuccess       ${version2_before}   ${version2_after}

UpdateSuccess
    [Documentation] Compares the version before and after update, if versions do not match, PASS
    [Arguments]     ${before}  ${after}
    ${res} =        versionEqual    ${before}   ${after}
    Run Keyword if        ${res} == 1           Log to Console      PASS
    Run Keyword if	      ${res} == 0       	Log to Console      FAIL


### CHECK SIG ##################################################################
App Signature Failure
    [Documentation]                     Checks that the OTA rejects a file if it does not have the correct app signature. For app, input HVAC file.
    [Arguments]                         ${pbc_ip}   ${app}
    Establish ssh connection            ${pbc_ip}
    Move app file               ${app_local}             ${app}
    Modify release.json         ${app}              ${release_path}          ${app_dir_remote}
    ${version1_before}=  Get App Version PBC         ${nordic1}
    ${version2_before}=  Get App Version PBC         ${nordic2}
    Trigger OTA Intentional Fail                 ${pbc_ip}
    ${version1_after}=  Get App Version PBC         ${nordic1}
    ${version2_after}=  Get App Version PBC         ${nordic2}
    Log to Console        Check App Sig:
    PassFail              ${version1_after}   ${version1_before}
    PassFail              ${version2_after}   ${version2_before}


PassFail
    [Documentation]       Checks to make sure that the version has not changed, which indicates the HVAC file was rejected
    [Arguments]           ${after}  ${before}
    ${res} =              versionEqual          ${after}  ${before}
    Run Keyword if	      ${res} == 1       	Log to Console      FAIL
    Run Keyword if        ${res} == 0           Log to Console      PASS


Trigger OTA Intentional Fail
    [Documentation]                     Trigger the OTA in telnet and only waits 60s becausef failure to OTA is anticipated
    [Arguments]                         ${pbc}
    Telnet.Open Connection              ${pbc}  port=2000
    SSHLibrary.Execute Command          systemctl restart prudmxagent
    SSHLibrary.Start Command            journalctl -f
    sleep                               60s
    Telnet.Write                        @gateway SET_OTA=X.X.X
    sleep                               60s


# RESET
Reset Nordic
    [Documentation]     Counts the number of companions connected, resets the nordic specified, then counts the number of companions connected after and compares. Pass if CM number matches.
    [Arguments]         ${pbc_ip}   ${MAC}
    ${result_before} =         Wait Until Keyword Succeeds  45s  1s    run process     ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int     shell=yes
    ${cm_before} =      numCompanions   ${result_before.stdout}
    Run keyword and ignore error  run process         ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} reset  shell=yes    timeout=1s  on_timeout=kill
    sleep   10s
    run process         ssh root@${pbc_ip} systemctl restart prudmxagent    shell=yes   #should connect automatically, 20s
    sleep   160s
    run process         ssh root@${pbc_ip} systemctl stop prudmxagent    shell=yes
    sleep   2s
    ${result_after} =         Wait Until Keyword Succeeds  45s  1s    run process     ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int     shell=yes
    ${cm_after} =      numCompanions   ${result_after.stdout}
    ${cmres} =            cmRepair    ${cm_before}    ${cm_after}
    Log to Console      Nordic Reset
    Run Keyword if      ${cmres} == 0       Log to Console      PASS
    Run Keyword if      ${cmres} == 1       Log to Console      FAIL


Reboot PBC
    [Documentation]     Reboots PBC
    [Arguments]         ${pbc_ip}
    start process       ssh root@${pbc_ip} reboot  shell=yes
    Sleep               45s


BL INT
    [Arguments]                        ${MAC}   ${pbc_ip}
    Run Keyword And Ignore Error    SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5  timeout=5s
    sleep                     3s
    ${result} = SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} int   return_stdout=True   timeout=2s
    ${Version} =                extractGWVersion    ${result}
    Log to Console              ${MAC}: ${Version}
    [Return]                    ${Version}

Enter BL
   [Arguments]                         ${MAC}  ${pbc_ip}
   Run Keyword And Ignore Error         SSHLibrary.Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5    timeout=5s
#   SSHLibrary.Write          /home/rpe/cgw-testtool /dev/ttyS2 ${MAC} resetA5 & pid=$!; sleep 1; kill "$pid"
   sleep                     1s
#   SSHLibrary.Write          kill "$pid"

Counter
    [Arguments]                 ${in}
    ${in} =                     Evaluate  ${in}+1

Print Results
    [Arguments]                 ${var}
    ${finalVar} =               Evaluate    ${var} + ${var} + 1
    Log to Console          Out of ${finalVar} cases, ${failures} cases failed

Check Status
    [Arguments]                         ${uid}
    ${result} =                         Wait Until Keyword Succeeds  1 minute    30ms   Execute Command          /home/rpe/cgw-testtool /dev/ttyS2 ${uid} int   return_stdout=True  timeout=1s
#    Log to Console                      ${result}
    ${status}=                          checkNordicStatus                   ${result}
    ${companions}=                      numCompanions                       ${result}
#    Log to console                      ${status}
#    Log to console                      ${companions}
    [return]                            ${status}   ${companions}

Perform Reset
#    Reboot PBC                          ${pbc_ip}
    [Arguments]                         ${nordic}   ${pbc_ip}
    Establish ssh connection            ${pbc_ip}
    Sleep                               3s
    Reset Nordic                        ${nordic}
    Sleep                               15s
    Establish ssh connection            ${pbc_ip}
#    Sleep                               10s
    ${nordicStatus}    ${companions}=  Check Status                        ${nordic}
#    Log to Console                      ${nordic} Status: ${nordicStatus}
#    Log to Console                      ${nordic} Companions: ${companions}

#    ${res} =                            CompanionNuum       ${companions}   3
    run keyword if                      ${companions} == 3         Log to Console  PASS
    run keyword if                      ${companions} != 3         Log to Console  FAIL

Restart prudmxagent
    [Arguments]                         ${pbc_ip}
    run process                         ssh root@${pbc_ip} systemctl restart prudmxagent    shell=yes


Stop prudmxagent
    [Arguments]                         ${pbc_ip}
    run process                         ssh root@${pbc_ip} systemctl stop prudmxagent    shell=yes
    Sleep                               2s

Nordic Connection
    [Arguments]                         ${pbc_ip}
    Establish ssh connection            ${pbc_ip}
    SSHLibrary.write                    journalctl -f
    ${out} =                            Run keyword and continue on failure        SSHLibrary.read until regexp         Warning: Gateway
#    ${out} =                            Run keyword and continue on failure        SSHLibrary.read until regexp         Discovered nordics:
    [return]                              ${out}

Serial Connection Test
    [Arguments]                         ${pbc_ip}
    Establish ssh connection            ${pbc_ip}
    Restart prudmxagent                 ${pbc_ip}
    ${out} =                            Nordic Connection                   ${pbc_ip}
    ${res} =                            didNordicsConnect                   ${out}
    Run Keyword if          ${res} == 0     Log to console    PASS
    Run Keyword if          ${res} == 1     Log to console    FAIL
    Run Keyword if          ${res} == 1     Reboot PBC        ${pbc_ip}
    Run Keyword if          ${res} == 1     Fail              Nordics did not reconnect
    SSHLibrary.close all connections

Interrogate
    [Arguments]                         ${pbc_ip}   ${nordic}
    ${result} =    run process  ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${nordic} int  shell=yes  timeout=10s
#    ${result} =    run process  ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 C8B1C66CBB9F int  shell=yes  timeout=2s
    Sleep           2s
    [return]        ${result.stdout}

Reboot Test
    [Arguments]                         ${pbc_ip}   ${nordic}
    [Timeout]                           300s
    Sleep                               30s
    Stop prudmxagent                    ${pbc_ip}
    ${out_before} =                     Interrogate                         ${pbc_ip}   ${nordic}
    Reboot PBC                          ${pbc_ip}
    Stop prudmxagent                    ${pbc_ip}
    ${out_after} =                      Interrogate                         ${pbc_ip}   ${nordic}
    ${out_before_CM}=                   numCompanions   ${out_before}
    ${out_after_CM} =                   numCompanions   ${out_after}
    ${status} =                         RebootEvaluateTest     ${out_before_CM}    ${out_after_CM}
    Run Keyword if                      ${status}== 0           Log to console  PASS
    Run Keyword if                      ${status}== 1           Log to console  FAIL
    Run Keyword if                      ${status}== 1           Reboot PBC        ${pbc_ip}
    Run Keyword if                      ${status}== 1           Fail              Companions did not reconnect to Nordics after PBC reboot


Nordic Reset Test
    [Arguments]                         ${pbc_ip}   ${nordic}
    [Timeout]                           300s
#    Sleep                               30s
    Stop prudmxagent                    ${pbc_ip}
    ${out_before} =                     Interrogate                         ${pbc_ip}   ${nordic}
    ${reset_handle} =    start process  ssh root@${pbc_ip} /home/rpe/cgw-testtool /dev/ttyS2 ${nordic} reset  shell=yes
    terminate process                   ${reset_handle}
    Restart prudmxagent                 ${pbc_ip}
    Sleep                               20s
    Stop prudmxagent                    ${pbc_ip}
    ${out_after} =                      Interrogate                         ${pbc_ip}   ${nordic}
    ${out_before_CM}=                   numCompanions   ${out_before}
    ${out_after_CM} =                   numCompanions   ${out_after}
    ${status} =                         RebootEvaluateTest     ${out_before_CM}    ${out_after_CM}
    Run Keyword if                      ${status}== 0           Log to console  PASS
    Run Keyword if                      ${status}== 1           Log to console  FAIL
    Run Keyword if                      ${status}== 1           Reboot PBC        ${pbc_ip}
    Run Keyword if                      ${status}== 1           Fail              Companions did not reconnect after nordic reset


Check Bootpart
    [Documentation]     Checks Bootpart value
    [Arguments]         ${pbc_ip}
    # Check bootpart value before
    ${boot_res1} =      run process  ssh root@${pbc_ip} sudo fw_printenv | grep "bootpart\="  shell=yes
    ${boot1} =      	Get Substring		 ${boot_res1.stdout}  9
    Log to Console      Bootpart Before Power Cycle: ${boot1}
    # Reboot PBC, or replace with Power Cycle
    Log to Console      Rebooting ...
    reboot pbc          ${pbc_ip}
    # Check bootpart value after
    ${boot_res2} =      run process  ssh root@${pbc_ip} sudo fw_printenv | grep "bootpart\="  shell=yes
    ${boot2} =      	Get Substring		 ${boot_res2.stdout}  9
    Log to Console      Bootpart After Power Cycle: ${boot2}
    # Logical argument, determine whether boot_res values are the same
    Run Keyword if      ${boot1} == ${boot2}      Log to Console  PASS
    Run Keyword if      ${boot1} != ${boot2}      Log to Console  FAIL
    Run Keyword if      ${boot1} != ${boot2}      Fail  bootpart before does not match bootpart after power cycle

Linux OTA
    [Documentation]     Updates PBC Linux FW to specified version
    [Arguments]         ${pbc_ip}   ${version}
#    run process         ssh-keygen -R ${pbc_ip}
    ${ver_before}       Get PBC Version  ${pbc_ip}
    Log to Console      Version before update: ${ver_before}
    Run Keyword if      '${ver_before}' == '${version}'         Log to Console  Already on ${version}
    Telnet.Open Connection              ${pbc_ip}  port=2000
    Telnet.Write                        SET_OTA=${version}
    Log to Console      Updating...
    Sleep               400s
    ${ver_after}        Get PBC Version  ${pbc_ip}
    Log to Console      Version after update: ${ver_after}
    Run Keyword if      '${ver_after}' == '${version}'      Log to Console  PASS
    Run Keyword if      '${ver_after}' != '${version}'      Log to Console  FAIL
    Run Keyword if      '${ver_after}' != '${version}'      FAIL            PBC did not update


Get PBC Version
    [Documentation]             Reads PBC Version
    [Arguments]                 ${pbc_ip}
    SSHLibrary.Open Connection  ${pbc_ip}              encoding=latin-1    timeout=15s
    SSHLibrary.Login            rpe   password=rpe
    ${ver} =                    SSHLibrary.Execute Command   sudo cat /version.txt     return_stdout=True  return_stderr=False  return_rc=False  sudo=True  sudo_password=rpe
    ${return_ver} =	            Get Substring	${ver}	12  -2
    [return]                    ${return_ver}

202 Change Release
    [Documentation]             changes the release to whatever track you input
    [Arguments]                 ${pbc_ip}   ${track}

#------------------------------------------------------------------------------------------------------------------------------------------------
# Host, load state, mode state, power readings
Turn Load Off
    [Documentation]             Turns specified load off
    [Arguments]                 ${host_ip}   ${load}
    run process                 curl http://${host_ip}:3050/control/energy/loads/${load}/Off  shell=yes
    Sleep                       5s
    Log to Console              \n${load} OFF

Turn Load On
    [Documentation]             Turns specified load on
    [Arguments]                 ${host_ip}   ${load}
    run process                 curl http://${host_ip}:3050/control/energy/loads/${load}/On  shell=yes
    Sleep                       5s
    Log to Console              \n${load} On

Check Power Reading
    [Documentation]             Returns load status
    [Arguments]                 ${host_ip}   ${load}
    ${power} =                  run process     curl http://${host_ip}:3050/states/Energy.Circuit.${load}.Power    shell=yes
    ${power_final} =            Get JSON Value      ${power.stdout}      data
    Log to Console              \n${load} Power: ${power_final} watts
    [return]                    ${power_final}

Check Energy Mode
    [Documentation]             Checks energy mode
    [Arguments]                 ${host_ip}
    ${out} =                    run process     curl http://${host_ip}:3050/states/Energy.ModeName    shell=yes
    ${mode} =                   Get JSON Value      ${out.stdout}      data
    Log to Console              \nCurrent Mode: ${mode}
    [return]                    ${mode}

Get JSON Value
    [Documentation]             Returns the JSON contents of field from JSON object
    [Arguments]                 ${json obj}     ${field}
    ${string json} =            evaluate    json.loads('''${json obj}''')    json
    ${result} =	                Set Variable    ${string json['${field}']}
    [return]                    ${result[0]}

Get Battery Operating Mode
    [Documentation]             Queries the host for battery operating mode, converts mode code to code name
    [Arguments]                 ${host_ip}
    ${out} =                    run process     curl http://${host_ip}:3050/states/Energy.Battery.OperatingMode  shell=yes
    ${BatOpMode} =              Get JSON Value      ${out.stdout}      data
    ${BatOpModeName} =          Run Keyword if              ${BatOpMode}[0] == 0        Set Variable    IDLE
    ${BatOpModeName} =          Run Keyword if              ${BatOpMode}[0] == 1        Set Variable    Charging
    ${BatOpModeName} =          Run Keyword if              ${BatOpMode}[0] == 2        Set Variable    Discharging
    ${BatOpModeName} =	        Set Variable If
    ...	${BatOpMode} == 0   IDLE
    ...	${BatOpMode} == 1	Charging
    ...	${BatOpMode} == 2	Discharging
    Log to console              \nBattery is ${BatOpModeName}
    [return]                    ${BatOpModeName}
#------------------------------------------------------------------------------------------------------------------------------------------------
